<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('Admin')->insert([
'name'=>'Admin',
'email'=>'admin@gmail.com',
'password'=>Hash::make('1234abcd'),
        ]);
    }
}
