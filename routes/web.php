<?php

use Illuminate\Support\Facades\Route;

Route::middleware('auth')->group(function(){


Route::get ('1',function(){
  return view('data');
});
Route::get ('Auth',function(){
  return view('template');
});

Route::post('user', 'App\Http\Controllers\scrum1Controller@verifie');
  //->middleware('App\Http\Middleware\Auth');

Route::get ('aceuill',function(){
  $filiere=\App\Models\filiere::all();
  $niveau=\App\Models\niveau::all();
  $eleve=\App\Models\eleve::paginate(5);
  return view('sal7',compact('eleve','niveau','filiere')); 
});

  //->middleware('App\Http\Middleware\Auth');

Route::get('ajouter','App\Http\Controllers\scrum1Controller@ajouter');
Route::post('insert','App\Http\Controllers\useController@store')->name('insert');
Route::get('/', function(){
    return view('layout.head');
   });
Route::post('chercher', 'App\Http\Controllers\scrum1Controller@chercher');
Route::get('chercher', 'App\Http\Controllers\scrum1Controller@chercher');
   //Route::post('supprimer/{id}','App\Http\Controllers\Controller1@supprimer');
  // Route::get('modifier/{}','App\Http\Controllers\Controller1@modifier');
 /*  Route::get('/', function(){
return view('aceuil');
  }); */
Route::get('/', function(){
    return view('eleve');
      });
Route::post('chercher', 'App\Http\Controllers\scrum1Controller@chercher');

Route::delete('delete/{id}', 'App\Http\Controllers\useController@destroy');

Route::get('users/{id}/edit/', 'App\Http\Controllers\useController@edit');

Route::patch('/eleve/{eleve}', 'App\Http\Controllers\scrum1Controller@update');


//salle
Route::get ('salle','\App\Http\Controllers\sController@index')->name('salle');


Route::post('insertS','App\Http\Controllers\sController@store');


Route::delete('/deleteS/{id}', 'App\Http\Controllers\sController@destroy');

Route::get('/salle/{id}/edit', 'App\Http\Controllers\sController@edit');




//filiere

Route::get ('filiere','\App\Http\Controllers\fController@index')->name('filiere');

  //->middleware('App\Http\Middleware\Auth');


Route::post('insertF','App\Http\Controllers\fController@store');
Route::post('insertC','App\Http\Controllers\fController@insert');


Route::delete('/filiere/{id}', 'App\Http\Controllers\fController@destroy');

Route::get('/filiere/{id}/edit', 'App\Http\Controllers\fController@edit');
Route::get('/filiere/{id}/classe', 'App\Http\Controllers\fController@classe');


//utilisateur

Route::get ('utilisateur','\App\Http\Controllers\UController@index')->name('utilisateur');

Route::post('regi','App\Http\Controllers\UController@store')->name('regi');

Route::delete('/utilisateur/{id}', 'App\Http\Controllers\UController@destroy');

Route::get('/utilisateur/{id}/edit', 'App\Http\Controllers\UController@edit');

//niveau


Route::get ('niveau','\App\Http\Controllers\nController@index')->name('niveau');




Route::post('insertN','App\Http\Controllers\nController@store');

Route::delete('/niveau/{id}', 'App\Http\Controllers\nController@destroy');

Route::get('/niveau/{id}/edit', 'App\Http\Controllers\nController@edit');





//session
Route::get ('session','\App\Http\Controllers\seController@index')->name('session');

Route::post('insertR','App\Http\Controllers\seController@store');


Route::delete('/session/{id}', 'App\Http\Controllers\seController@destroy');

Route::get('/session/{id}/edit', 'App\Http\Controllers\seController@edit');
Route::get('/session1/{id}/edit1', 'App\Http\Controllers\seController@edit1');
Route::get('/RAE', 'App\Http\Controllers\numController@edit2');

Route::get('/session/{id}','App\Http\Controllers\seController@parametrageS');

Route::patch('/insertP/{session}','App\Http\Controllers\scrum1Controller@insertP');


//import
Route::get('/import1',function(){
  return view('import');
})->name('import1');

Route::post('/import','App\Http\Controllers\scrum1Controller@import')->name('import');
Route::post('/salle','App\Http\Controllers\scrum1Controller@importS');
Route::post('/session','App\Http\Controllers\scrum1Controller@importSe');
Route::post('/filiere','App\Http\Controllers\scrum1Controller@importF');
Route::post('/niveau','App\Http\Controllers\scrum1Controller@importN');
Route::post('/departement','App\Http\Controllers\scrum1Controller@importD');


//export
Route::get('/export',function(){
  return view('export');
});
Route::get('/exportEleve','App\Http\Controllers\scrum1Controller@export');
Route::get('/exportSalle','App\Http\Controllers\scrum1Controller@exportS');
Route::get('/exportSession','App\Http\Controllers\scrum1Controller@exportSe');
Route::get('/exportFiliere','App\Http\Controllers\scrum1Controller@exportF');
Route::get('/exportNiveau','App\Http\Controllers\scrum1Controller@exportN');
Route::get('/exportDep','App\Http\Controllers\scrum1Controller@exportD');
Route::get('/exportEleveC','App\Http\Controllers\scrum1Controller@exportCSV');
Route::get('/exportModels','App\Http\Controllers\scrum1Controller@exportModels');


//departement
Route::get ('departement','\App\Http\Controllers\dController@index')->name('departement');

Route::post('insertD','\App\Http\Controllers\dController@store');

Route::delete('/departement/{id}', '\App\Http\Controllers\dController@destroy');

Route::get('/departement/{id}/edit', '\App\Http\Controllers\dController@edit');






//new
Route::get('/importNew','\App\Http\Controllers\scrum1Controller@importFile');
Route::post('/importNew','\App\Http\Controllers\scrum1Controller@importExcel');


//havsa
Route::get('/livetable','\App\Http\Controllers\controler1@index')->name('/ex1');
Route::get('deleteE','\App\Http\Controllers\controler1@deleteE')->name('deleteE');
Route::get('/livetable/fetch_data','\App\Http\Controllers\controler1@fetch_data');
Route::get('/2',function(){
  $depar=\App\Models\departement::all();
  $niveau=\App\Models\niveau::all();
  $filiereMqi=\App\Models\filiere::where('Nom','=','MQI');
  $filiereMed=\App\Models\filiere::where('Nom','=','MED');
  return view('dataTableExample',compact('depar','niveau','filiereMqi','filiereMed'));
});



Route::get('use', '\App\Http\Controllers\useController@index')->name('users.index');
//->middleware('auth');


//Route::get('users/{id}/edit/','\App\Http\Controllers\UserController@edit');




Route::post('parametrage','App\Http\Controllers\seController@parametrage');

Route::get('/check', function(){
  return view('checkbox');
});
Route::get('/test1', function(){
  return view('test1');
});
Route::post('checkbox','\App\Http\Controllers\scrum1Controller@insertCheckbox');
Route::post('meme','\App\Http\Controllers\scrum1Controller@insertMeme');

Route::post('insertSS','\App\Http\Controllers\controler1@store');

Route::get('/attr/{id}', '\App\Http\Controllers\controler1@index')->name('attr');
Route::get('/repar1', '\App\Http\Controllers\algoC@index')->name('repar1');

Route::delete('/sessionsalle/{id}', '\App\Http\Controllers\controler1@destroy');
Route::get('/sessionsalle/{id}/edit', '\App\Http\Controllers\controler1@edit');

Route::get('/page', function(){

  return view('test');
});
Route::get('/bd', function(){

  return view('test3');
});
Route::post('mewjoud','App\Http\Controllers\seController@verification');
Route::get('repartition/{id}','App\Http\Controllers\algoSl7@test');
Route::get('fetch/{id}','App\Http\Controllers\seController@fetch_test');
Route::get('memet/{id}','App\Http\Controllers\seController@recupmemet');

Route::get('export','\App\Http\Controllers\ImportExportController@export')->name('export');
Route::get('importExportView','\App\Http\Controllers\ImportExportController@importExportView');

});

Auth::routes();

Route::get('/home','App\Http\Controllers\HomeController@index')->name('home');



Route::get('logout','App\Http\Controllers\Auth\LoginController@logout');


