jQuery(function () 
{ var larails = { 
    // Deﬁne the name of the hidden input ﬁeld for method submission 
    methodInputName: '_method',
     // Deﬁne the name of the hidden input ﬁeld for token submission 
     tokenInputName: '_token', 
     // Deﬁne the name of the meta tag from where we can get the csrf-token 
     metaNameToken: 'csrf-token', 
     initialize: function() { 
         $('a[data-method]').on('click', this.handleMethod); 
        },
         handleMethod: function(e) 
         { 
             e.preventDefault(); 
             var link = $(this), 
             httpMethod = link.data('method').toUpperCase(), 
             conﬁrmMessage = link.data('conﬁrm'), 
             form; 
             // Exit out if there is no data-methods of PUT , PATCH or DELETE. 
             if ($.inArray(httpMethod, ['PUT', 'PATCH', 'DELETE']) === -1) 
             { 
                 return; 
                } 
                // Allow user to optionally provide data-conﬁrm="Are you sure?" 
                if (conﬁrmMessage) { 
                    if( conﬁrm(conﬁrmMessage) ) 
                    { 
                        form = larails.createForm(link); 
                        form.submit(); 
                    } 
                } else { 
                    form = larails.createForm(link); 
                    form.submit(); 
                } 
            }, 
            createForm: function(link)
             { 
                 var form = $('<form>', 
                 { 
                     'method': 'POST', 
                     'action': link.prop('href') 
                    }); 
                    var token =$ ('<input>', 
                    {
                         'type': 'hidden', 
                         'name': larails.tokenInputName, 
                         'value': $('meta[name=' + larails.metaNameToken + ']').prop('content') 
                        }); 
                        var method = $('<input>', 
                        { 
                            'type': 'hidden', 
                            'name': larails.methodInputName, 
                            'value': link.data('method') 
                        }); 
                        return form.append(token, method).appendTo('body'); 
                    } 
                }; 
                larails.initialize();
            });
