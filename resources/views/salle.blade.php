<!DOCTYPE html>
<html>
<head>
    <title>Salle</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    


    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.css"/>
      <script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script> 
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<style>
pre{
    background-color:white;
    border:0;
}
</style>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <ul class="nav navbar-nav">
    @if(Auth::user()->isAdmin)<li class="nav-item"><a href="utilisateur">Utilisateur</a></li>@endif

    <li class="nav-item" id="id1"><a  class="nav-link" href="use">Etudiants</a></li>
    <li class="nav-item"><a href="filiere">Filières</a></li>
    <li class="nav-item"><a href="niveau">Niveaux</a></li>
    <li class="nav-item"><a href="departement">Départements</a></li>
    <li  class="nav-item active"><a href="salle">Salles</a></li>
    <li class="nav-item"><a href="session">Sessions</a></li>
    </ul>

    <ul class="nav navbar-nav navbar-right">
    <li>
   
   <a  href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
      <span class="glyphicon glyphicon-log-in"></span> Se Déconnecter
   </a>

   <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
       @csrf
   </form>

</li>
    </ul>
    </nav>
<button type="button" class="btn btn-white" Onclick=window.location.href='page' >
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-house-door-fill" viewBox="0 0 16 16"> <path d="M6.5 14.5v-3.505c0-.245.25-.495.5-.495h2c.25 0 .5.25.5.5v3.5a.5.5 0 0 0 .5.5h4a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4a.5.5 0 0 0 .5-.5z"/> </svg>
    </button>
    <div id="alert_message"></div>
@if($errors->any())
    <div class="alert alert-danger">
    <ul>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
<span class="sr-only" >Close</span>
</button>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
    </ul>
    </div>
    @endif
  
    @if(Session::has('success'))
<div class="alert alert-success alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
<span class="sr-only" >Close</span>
</button>
<strong>{{ Session::get('success')}}</strong>
</div>
  @endif  

  @if(Session::has('error'))
<div class="alert alert-danger alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
<span class="sr-only" >Close</span>
</button>
<strong>{{ Session::get('error')}}</strong>
</div>
  @endif

<div class="container">
<h1 align="center">Les salles</h1>
<br/>
<div class="row">
<div class="col-lg-12 margin-tb">
<div class="pull-right">
<a class="btn btn-success mb-2" id="new-user" data-toggle="modal"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
  <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
  <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
</svg></a>
</div>
</div>
</div>
   <pre></pre> 

<div class="panel panel-primary">
      <div class="panel-heading" align="center">Liste Des Salles</div>
      <div class="panel-body">
    <table  class="table table-bordered data-table">
        <thead>
            <tr>
                <th>Nom salle</th>
                <th>Capacité</th>
                
                <th width="100px">Action</th>
            </tr>
            </thead>
        <tbody>
        </tbody>
    </table>
</div>
    <div class="modal fade" id="crud-modal" aria-hidden="true" >
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">

<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title" id="userCrudModal"></h4>
</div>
<div class="modal-body">
<form name="userForm" action="insertS" method="POST">
<input type="hidden" name="user_id" id="user_id" >
@csrf
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>Nom:</strong>					
<input type="text" name="Nom" id="nom" placeholder="Entrer le Nom du salle" class="form-control"  />
						</div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>Capacité:</strong>
<input type="text" name="capacite" placeholder="Entrer le capacité du salle" class="form-control" id="capacite"/>
						</div>
                        </div>
           

<div class="col-xs-12 col-sm-12 col-md-12 text-center">
<button type="submit" id="btn-save" name="btnsave" class="btn btn-primary" >Enregistrer</button>
<a href="{{ route('salle') }}" class="btn btn-danger">Annuler</a>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
   
</body>
   
<script type="text/javascript">
  $(function () {
    
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('salle') }}",

        dom:'lBfrtip',
        buttons: [
            'excel', 'csv', 'pdf'
        ],
        "lengthMenu":[[3,5,10,25,50,100,-1],[3,5,10,25,50,100,"All"]],
        columns: [
            {data: 'Nom', name: 'Nom'},
            {data: 'Capacite', name: 'Capacite'},
           
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        "language":{
    "sProcessing":     "Traitement en cours...",
    "sSearch":         "Rechercher&nbsp;:",
    "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
    "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
    "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
    "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
    "sInfoPostFix":    "",
    "sLoadingRecords": "Chargement en cours...",
    "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
    "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
    "oPaginate": {
        "sFirst":      "Premier",
        "sPrevious":   "Pr&eacute;c&eacute;dent",
    
        "sNext":       "Suivant",
    
        "sLast":       "Dernier"
        }}
    });
  });
  </script>
<script type="text/javascript">
  $(function () {
    $('#new-user').click(function () {
$('#btn-save').val("create-user");
$('#user').trigger("reset");
$('#userCrudModal').html("Ajouter un salle");
$('#crud-modal').modal('show');
});

$('body').on('click', '#edit-user', function () {
var user_id = $(this).data('id');
$.get('salle/'+user_id+'/edit', function (data) {
$('#userCrudModal').html("Modifier salle");
$('#btn-update').val("Update");
$('#btn-save').prop('disabled',false);
$('#crud-modal').modal('show');
$('#user_id').val(data.id);
$('#nom').val(data.Nom);
$('#capacite').val(data.Capacite);


})
});

$('body').on('click', '#delete-user', function () {
var user_id = $(this).data("id");
var token = $("meta[name='csrf-token']").attr("content");
if(confirm("êtes Vous Sûr?")){

   $.ajax({
    url: 'deleteS/'+user_id,
type: 'DELETE',

data: {
"id": user_id,
"_token": token,
},
success:function(data)
       {
         $('#alert_message').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span><span class="sr-only" >Close</span></button>salle a été supprimée</div>');
         table.ajax.reload();
       },
       error: function (data) {
  $('#alert_message').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span><span class="sr-only" >Close</span></button>il y avait une erreur<div>');
console.log('Error:', data);
}
})
}

});

    
  });
</script>
</html>