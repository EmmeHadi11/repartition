<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html>
<head>
	<title>Animated Dynamic Form</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<style>


   
	html,body{
			height: 100%;
			margin: 0;
		
		}
   
   .myForm{
   	background-color: rgba(0,0,0,0.5);
   	padding: 15px;
   border-radius: 15px ;
   color: white;
   
   }

   input{
   	border-radius:0 15px 15px 0 ;

   }
   input:focus{
      
box-shadow:none ;
border:1px solid #ccc ;

   }

   .br-15{
   	border-radius: 15px 0 0 15px ;
   }

   #add_more{
   	color: white ;
   	background-color: #fa8231 ;
   	border-radius: 15px ;
   	border: 0 ;

   }
   #remove_more{
   	color: white ;
   	background-color: #fc5c65 ;
   	border-radius: 15px ;
   	border: 0 ;
   	display: none;

   }
   	
   .submit_btn{
   	border-radius: 15px;
    background-color: #95c714;
    border: 0;
   }
  
pre{
    color:black;
}

   </style>
   <script>
   </script>
</head>

<!-- Coded With Love By Mutiullah Samim-->
<body>
	<div class="container h-100">
	<div class="d-flex justify-content-center">
		<div class="card mt-5 col-md-4 ">
			<div class="card-header">
				<h4>Paramétrér session</h4>
			</div>
			<div class="card-body">
            <form method="post" action="/insertP/{{$session->id}}" >
    @csrf
    @method('PATCH')
    
      
					<div id="dynamic_container">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text br-15"><i class="fas fa-user-graduate"></i></span>
							</div>
							
            <input type="text" name="id" class="form-control"  placeholder="type session" value="{{$session->id}}" required>
						</div>

                    
                       
<legend> choissez les filiére et les niveaux en Licence:</legend>
@foreach($filiere as $fil)

<pre><div><label>Filiére:</label> <input type="checkbox" name="case2[]" /><?php echo $fil->Nom; ?>  <label >Niveaux:</label>@foreach($niveau as $niv)<input type="checkbox" name="case1[]"/><?php echo $niv->Nom; ?> @endforeach</div></pre>

@endforeach

<legend> choissez les filiére et les niveaux en Master:</legend>
@foreach($master as $mas)

<pre><div><label>Filiére:</label> <input type="checkbox" name="case2[]" /><?php echo $mas->Nom; ?>  <label >Niveaux:</label>@foreach($masterniv as $niv)<input type="checkbox" name="case1[]"/><?php echo $niv->Nom; ?> @endforeach</div></pre>

@endforeach
                 
			


			
			

			<div class="card-footer">
			<input type="submit"  Value="Soumettre" class="btn btn-primary"/>


			</form>

			</div>
		</div>
	</div>
	</div>
    <script type="text/javascript">

function Cacher(element) { document.getElementById(element).style.visibility = "hidden"; }

function Visible (element) { document.getElementById(element).style.visibility = "visible"; }

function Toggle(element)

{

    if (document.getElementById(element) == document.getElementById('fill')){

        Visible('fill');

        //Cacher('modifier');

    }

  /* else    {  

        Visible('modifier');   

        Cacher('ajouter');

    }

}*/

//window.onload = function () {Cacher('element2');    };
function exemple(){

}


</script>

</body>
</html>