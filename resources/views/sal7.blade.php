<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    
    <!--Fontawesome CDN-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<meta name="csrf-token" >

<style>
    <table class="table table-dark">
<tr>
<td>idSession</td>
<td>Matricule</td>
<td>Numéro</td>

</tr>

@foreach($numEx as $num)
<tr>
<td><?php echo $num->idSession; ?></td>
<td><?php echo $num->matricule; ?></td>
<td><?php echo $num->numero; ?></td>
</tr>
  @endforeach
    </table>
#ajouter{
    visibility:hidden;
    }
   
#modifier{
    visibility:hidden;
    }   



    html,body{
			height: 100%;
			margin: 0;
			background: rgb(2,0,36);
            background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(149,199,20,1) 0%, rgba(0,212,255,1) 96%);
		
		}
   
   .myForm{
   	background-color: rgba(0,0,0,0.5) !important;
   	padding: 15px !important;
   border-radius: 15px !important;
   color: white;
   
   }

   input{
   	border-radius:0 15px 15px 0 !important;

   }
   input:focus{
       outline: none;
box-shadow:none !important;
border:1px solid #ccc !important;

   }

   .br-15{
   	border-radius: 15px 0 0 15px !important;
   }

   #add_more{
   	color: white !important;
   	background-color: #fa8231 !important;
   	border-radius: 15px !important;
   	border: 0 !important;

   }
   #remove_more{
   	color: white !important;
   	background-color: #fc5c65 !important;
   	border-radius: 15px !important;
   	border: 0 !important;
   	display: none;

   }
   	
   .submit_btn{
   	border-radius: 15px !important;
    background-color: #95c714 !important;
    border: 0 !important;
   }
   </style>
</style>

</head>
<?php 

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
?>
<body style="background-image: url('/images/bg1.jpg'); background-size:cover;">

@if($errors->any())
    <div class="alert alert-danger">
    <ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
    </ul>
    </div>
    @endif
  
    <button type="button" class="btn btn-white" Onclick=window.location.href='page' >
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-house-door-fill" viewBox="0 0 16 16"> <path d="M6.5 14.5v-3.505c0-.245.25-.495.5-.495h2c.25 0 .5.25.5.5v3.5a.5.5 0 0 0 .5.5h4a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4a.5.5 0 0 0 .5-.5z"/> </svg>
    </button>
  
    <form action="chercher" method="POST">
    <br>
    @csrf
    <div class="input-group" style="left:10%; ">
  <div class="form-outline">
  <input id="search-focus" type="text" name="char" class="form-control" placeholder="Search"/>
    
  </div>
  
  <button type="submit" class="btn btn-primary" name="chercher">
  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
  <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
</svg>
  </button>
</div>
</div>
<div style=" margin-left:80%;">
  <button type="button" onclick="Toggle('ajouter');" class="btn btn-primary"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
  <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
  <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
</svg></button>
 <!-- href="ajouter"  -->
  </div>
  <br>
<table class="table table-striped table-dark" style="width:80%;  margin-left:20px;" >
  <thead>
    <tr>
    
      <th scope="col">Num d'inscription</th>
      <th scope="col">Nom Complet</th>
      <th scope="col">Filiere</th>
      <th scope="col">Niveau</th>
      <th scope="col" >Operations</th>
      
    </tr>
  </thead>
  
  <tbody>
    <tr>
    
    @foreach($eleve as $inf)
    
    
      <td><?php echo "$inf->NumInscr "; ?></td>
      <td><?php echo "$inf->NomComplet "; ?></td>
      <td><?php echo "$inf->filiere "; ?></td>
     
      <td><?php echo "$inf->niveau "; ?></td>
      <td><a href="/eleve/{{$inf->id}}/edit" onclick="Toggle('modifier');"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil" viewBox="0 0 16 16">
  <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
</svg></a>
      <a href="/eleve/{{$inf->id}}/delete"  onclick="return confirm('Etes vous sur?')"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16"> <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/> <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/> </svg>
</a>
</td>
    </tr>
    

 
@endforeach
  </tbody>
</table>




<div>{{ $eleve->links() }}</div>

    </form>


<div id="ajouter">
<div class="container h-100">
	<div class="d-flex justify-content-center">
		<div class="card mt-5 col-md-4 animated bounceInDown myForm">
    <div class="card-header">
     <button type="button" class="btn btn-danger float-right" onclick="Cacher('ajouter')" id="btn">
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16">
  <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
  <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
</svg>
    </button>
				<h4>Add Students</h4>
			</div>
			<div class="card-body">
				<form method="post" action="insert">
        @csrf
					<div id="dynamic_container">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text br-15"><i class="fas fa-user-graduate"></i></span>
							</div>
							<input type="text" name="matricule" placeholder="Entrer le matricule" class="form-control" />
						</div>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text br-15"><i class="fas fa-user-graduate"></i></span>
							</div>
							<input type="text" name="nom" placeholder="Entrer le Nom Complet" class="form-control" />
						</div>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text br-15"><i class="fas fa-user-graduate"></i></span>
							</div>
							<input type="date" name="dateN" placeholder="Entrer le date de Naissance" class="form-control" />
						</div>
						<div class="input-group mt-3">
							<div class="input-group-prepend">
              <span class="input-group-text br-15"><i class="fas fa-user-graduate"></i></span>
							</div>
							<input type="text" name="lieuN" placeholder="Entrer le lieu de Naiisance" class="form-control" />
						</div>

            <div class="input-group mt-3">
							<div class="input-group-prepend">
              <span class="input-group-text br-15"><i class="fas fa-user-graduate"></i></span>
							</div>
							<input type="text" name="nni" placeholder="Entrer le NNI" class="form-control" />
						</div>
            <div>Sexe<br>
						<input type="radio" name="sex" value="femme"/>femme<br>
            <input type="radio" name="sex" value="homme"/>homme<br>
            </div>

					
						<select name="choix">
    <option value="opt">choisir Niveau</option>
    @foreach($niveau as $niv)
   <option ><?php echo"$niv->Nom" ?></option>
    @endforeach
    </select>


    <select name="choixN">
    <option value="opt">choisir filiere</option>
    @foreach($filiere as $fil)
   <option ><?php echo"$fil->Nom" ?></option>
    @endforeach
    </select>
					</div>
			
			</div>
			<div class="card-footer">
			<input type="submit"  Value="Soumettre" class="btn btn-primary"/>


			</form>
 
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>

<script type="text/javascript">

function Cacher(element) { document.getElementById(element).style.visibility = "hidden"; }

function Visible (element) { document.getElementById(element).style.visibility = "visible"; }

function Toggle(element)

{

    if (document.getElementById(element) == document.getElementById('ajouter')){

        Visible('ajouter');

        Cacher('modifier');

    }

   else    {  

        Visible('modifier');   

        Cacher('ajouter');

    }

}

//window.onload = function () {Cacher('element2');    };

</script>


</body>
</html>