<!DOCTYPE html>
<html>
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    

    <title>Document</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    


<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.css"/>
  <script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script> 
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<style>
pre{
  background-color:white;
  border:0;
  
}

</style>

</head>
<body>

<button type="button" class="btn btn-white" Onclick=window.location.href='page' >
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-house-door-fill" viewBox="0 0 16 16"> <path d="M6.5 14.5v-3.505c0-.245.25-.495.5-.495h2c.25 0 .5.25.5.5v3.5a.5.5 0 0 0 .5.5h4a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4a.5.5 0 0 0 .5-.5z"/> </svg>
    </button>
@if($errors->any())
    <div class="alert alert-danger">
    <ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
    </ul>
    </div>
    @endif
  
    
    @if(Session::has('success'))
<div class="alert alert-success alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
<span class="sr-only" >Close</span>
</button>
<strong>{{ Session::get('success')}}</strong>
</div>
  @endif  

  @if(Session::has('error'))
<div class="alert alert-danger alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
<span class="sr-only" >Close</span>
</button>
<strong>{{ Session::get('error')}}</strong>
</div>
  @endif


<div class="modal fade" id="crud-modal" tabindex="-1" aria-labelledby="studentModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      <h5 class="modal-title" id="userCrudModal"></h5> 
<button type="button" class="close" data-dismiss="modal">&times;</button>
        
</div>
<div class="modal-body">
<form name="userForm" action="/insertSS/" method="POST">
<input type="hidden" name="user_id" id="user_id" >
@csrf
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>Nom Salle:</strong>					

<select id="liste" name="Nom">
  <option  selected="yes">
    choisir une salle
  </option>
@foreach($salle as $sal) 
<option name="{{$sal->Capacite}}">
<?php echo $sal->Nom; ?>
</option>
@endforeach
</select> 

						</div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
                        <strong>Capacite Utilisée:</strong>					
<input type="text" name="capacite" id="dd" placeholder="Entrer la capacité utilisée " class="form-control"  Readonly/>
						</div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
                        <strong>Num Début :</strong>					
<input type="text" name="numD" id="df" placeholder="Entrer le numéro de début" class="form-control"  />
						</div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
                        <strong>Num Fin:</strong>					
<input type="text" name="numF" id="an" placeholder="Entrer le numéro de fin " class="form-control"  />
<input type="hidden" name="idS" value="{{$id}}" />
						</div>
                        </div>        

<div class="col-xs-12 col-sm-12 col-md-12 text-center">

<button type="submit" id="btn-save" name="btnsave" class="btn btn-primary" >Save</button>
<a href="sessionSalle/{{$id}}" class="btn btn-danger">Cancel</a>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
<!-- Modal -->
<div class="modal fade" id="studentModal1"  tabindex="-1" aria-labelledby="studentModalLabel1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="studentModalLabel1">Export Liste</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
      <form action="{{ route('export') }}" method="GET" enctype="multipart/form-data">
                {{ csrf_field() }}
                <select name="salle">
                    @foreach($salles2 as $sal)
                    
                    <option class="form-control" >{{$sal->Nom}}</option>
                    
                    @endforeach
                </select>
                <br>
                <select name="classe">
                    @foreach($para as $pr)
                    
                    <option class="form-control" >{{$pr->NomClasse}}</option>
                    
                    @endforeach
                </select>
                <input type="hidden" name="idS" value="{{$id}}" class="form-control">
                <br>
               
            </form>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button class="btn btn-warning" type="submit">Export Liste salle</button>
      </div>
    </div>
  </div>
</div>
</div>
  <!-- end of modal export -->
    <div class="container">
<div class="row">

<div class="col-md-12">
 

<div id="alert_message"></div>
<div class="btn-toolbar pull-right">

<a data-bs-toggle="modal" data-bs-target="#studentModal1" id="exportListe" data-id='.$row->id.' class="btn btn-danger pull-right">Export Liste d'une salle</a>
<a href="#"  class="btn btn-success pull-right" id="new-user" data-toggle="modal"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
  <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
  <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
</svg></a>
</div>
<pre></pre>
<pre></pre>

<div class="panel panel-primary">
      <div class="panel-heading" align="center">Attribution des salles</div>
      <div class="panel-body">
<table id="example" class="table table-bordered ">
    <thead>
    <tr>
    <th>Nom</th>
    <th>Capacité</th>
    <th>numéro début</th>
    <th>numéro fin</th>
    <th width="120px">Action</th>
    </tr>
    </thead>

    <tbody id="tbody">
   
    </tbody>


    </table>
    </div>
</div>
</div>
</div>
</div>

</div>
   
</body>
   
<script type="text/javascript">
  $(document).ready(function () {

    var id=<?php echo $id; ?>;

  
fetch_data();
function fetch_data(){
    var table = $('#example').DataTable({
        processing: true,
        serverSide: true,
        ajax:{url:"/attr/"+id,
        data:{"id":id},
        dataType:'json'},
        "lengthMenu":[[5,10,25,50,100,-1],[5,10,25,50,100,"All"]],
        columns: [
            {data: 'Nom', name: 'Nom'},
            {data: 'capaciteUtilise', name: 'capaciteUtilise'},
            {data: 'numDebut', name: 'numDebut'},
            {data: 'numFin', name: 'numFin'},
           
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],


        "language":{
    "sProcessing":     "Traitement en cours...",
    "sSearch":         "Rechercher&nbsp;:",
    "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
    "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
    "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
    "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
    "sInfoPostFix":    "",
    "sLoadingRecords": "Chargement en cours...",
    "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
    "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
    "oPaginate": {
        "sFirst":      "Premier",
        "sPrevious":   "Pr&eacute;c&eacute;dent",
    
        "sNext":       "Suivant",
    
        "sLast":       "Dernier"
        }}
    });
}

    
//ajouter modal ajax
$('#new-user').click(function () {
$('#btn-save').val("create-user");
$('#user').trigger("reset");
$('#userCrudModal').html("Ajouter une session");
$('#crud-modal').modal('show');
});

//edit modal ajax
$('body').on('click', '#edit-user', function () {
var user_id = $(this).data('id');
$.get('/sessionsalle/'+user_id+'/edit', function (data) {
$('#userCrudModal').html("Modifier session");
$('#btn-update').val("Update");
$('#btn-save').prop('disabled',false);
$('#crud-modal').modal('show');
$('#user_id').val(data.id);
$('select[name="Nom"]').val(data.Nom);
$('#dd').val(data.capaciteUtilise);
$('#df').val(data.numDebut);
$('#an').val(data.numFin);

})
});

//delete 

$('body').on('click', '#delete-user', function () {
var user_id = $(this).data("id");
var token = $("meta[name='csrf-token']").attr("content");
if(confirm("Etes Vous Sur !")){

$.ajax({
    url: '/sessionsalle/'+user_id,
type: 'DELETE',

data: {
"id": user_id,
"_token": token
},
success: function (data) {
$('#alert_message').html('<div class="alert alert-success">session bien Supprimée<div>');
$('#example').DataTable().destroy();
         fetch_data();
},
error: function (data) {
  $('#alert_message').html('<div class="alert alert-danger">erreur dans la Suppréssion<div>');
console.log('Error:', data);
}
})
}
});

 $(document).on('change','#liste', function(){
    console.log('hi!');
    
     var $value = $('#liste option:selected').attr("name");
if($value!="1"){
  $('#dd').val($value);

  <?php foreach($salles as $sal){?>
var numD=<?php echo $sal->numFin; ?>+1;
<?php } ?>
$('#df').val(numD);
var numF= parseInt($value) +numD;

$('#an').val(numF);


}else{
  $('#dd').val('');
$('#df').val(' ');
$('#an').val(' ');
}
  
    
});

$('body').on('click','#exportListe',function (){
  $('#studentModal1').modal('show');
});
    
  });
</script>
</html>