
<!------ Include the above in your HEAD tag ---------->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
    <!--Fontawesome CDN-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<title>Login Page</title>

   <!--Made with love by Mutiullah Samim  https://getwallpapers.com/wallpaper/full/a/5/d/544750.jpg-->
   <style >

@import url('https://fonts.googleapis.com/css?family=Numans');




.card{
height: 350px;
margin-left: auto;
margin-right: auto;
margin:auto;
width: 400px;

}


.login_btn{
color: blue;
border-radius:30px;
border-color:blue;

}



.profile-img-card {
	
    width: 96px;
    height: 96px;
    margin: 0 auto 10px;
    display: block;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
	overflow:hidden ;
	top:calc(-100px/2);
	left:calc(50% - 50px);
	position:absolute;

}
#input{
	width:60%;
	height:25px;
	margin:0 auto;
	border:solid 1px #ccc;
	
border-radius:30px;
}
/*
 * Form styles
 */
.profile-name-card {
	
    font-size: 16px;
    font-weight: bold;
    text-align: center;
    margin: 10px 0 0;
    top:150px;
display:block;

}
.card-header{
	height:105px;
}
h3{
	top:calc(100px/2);
	text-align:center;
	color:blue;
    font-weight: bold;
}





</style>



   
		<!--Bootsrap 4 CDN-->



<body background="\public\img\IMG-20210511-WA0001.jpg">
<div class="container">
@if(Session::has('success'))
<div class="alert alert-success alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
<span class="sr-only" >Close</span>
</button>
<strong>{{ Session::get('success')}}</strong>
</div>
  @endif  

  @if(Session::has('error'))
<div class="alert alert-danger alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
<span class="sr-only" >Close</span>
</button>
<strong>{{ Session::get('error')}}</strong>
</div>
  @endif
	<div class="d-flex justify-content-center h-100">
		<div class="card">
			<div class="card-header">
				<img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
            <pre>

			 <h3>Connectez-vous</h3></pre>
			</div>
			<div class="card-body">
            
				<form method="post" action='user'> 
                @csrf
<label for="usr">
							Nom d'utilisateur:
						</label>
					<div class="input-group form-group inner-addon right-addon">
						<input id="#input" type="text" name="name" class="form-control" class="fas fa-user" placeholder="Saisir le nom d'utilisateur">
						
					</div>
					<label for="usr">
							Mot de passe:
							</label>
					<div class="input-group form-group inner-addon right-addon">
					
						<input type="password" name="pass" class="form-control class="fas fa-key" placeholder="Saisir le Mot de passe">
					</div>
					<div class="form-group">
						<input type="submit" value="Connecter" class="btn float-right login_btn border-raduis">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
