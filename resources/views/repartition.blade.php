<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    


    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.css"/>
      <script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script> 
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    
  <style>
pre{
    background-color:white;
    border:0;
}

  </style>
</head>
<body>
<pre></pre>
<div class="container">
<div id="repa">
<button class="btn btn-danger pull-right" id="un" >Voir les attributions des numéros</button>
<pre></pre>
<pre></pre>
<div class="panel panel-primary">
      <div class="panel-heading" align="center">Répartitions</div>
      <div class="panel-body">
    <table class="table table-dark table-bordered">
<tr>
<td>Filiére</td>
<td>Numéro du début</td>
<td>Numéro de Fin</td>


</tr>

@foreach($repartition as $repa)
<tr>
<td><?php echo $repa->idFiliere; ?></td>
<td><?php echo $repa->NumDebut; ?></td>
<td><?php echo $repa->NumFin; ?></td>

</tr>
  @endforeach
    </table>
</div>
</div></div>

<div id="unq">
<div class="pull-right">
<select id="liste">
<option >Choisir classe</option>
@foreach($memet as $mm)
<option id="<?php echo $mm->Classe; ?>"><?php echo $mm->Classe; ?></option>
@endforeach
    
</select>
<button class="btn btn-danger " id="repart" >Voir les répartitions</button>
</div>
<pre></pre>
<pre></pre>
<div class="panel panel-primary">
      <div class="panel-heading" align="center">Attributions des numéros</div>
      <div class="panel-body">
    <table class="table table-dark " id="tbd">
<tr>
<td>idSession</td>
<td>Nom complet</td>

<td>Matricule</td>
<td>Classe</td>

<td>Numéro</td>

</tr>

@foreach($numEx as $num)
<tr>
<td><?php echo $num->idSession; ?></td>
<td><?php echo $num->Nom; ?></td>

<td><?php echo $num->matricule; ?></td>
<td><?php echo $num->Classe; ?></td>

<td><?php echo $num->numero; ?></td>
</tr>
  @endforeach
    </table>
</div></div></div>
</div>
</body>
<script type="text/javascript">

$(function(){
  $('#unq').hide();

   $('#repart').click(function(){
  $('#unq').hide();
  $('#repa').show();

  });

  $('#un').click(function(){
  $('#repa').hide();
  $('#unq').show();

  });

  $(document).on('change','#liste', function(){
    var $value = $(this).val();
var token = $("meta[name='csrf-token']").attr("content");

$value1=<?php echo $idm; ?>

 

  $.ajax({
    url:'/RAE/',
    type:'GET',
    dataType:'json',

    data: {
 "id": $value1,
 "classe":$value,
 "_token": token,
 },

success:function(data){
 // alert(data.numero);
 var html='';
      html += '<tr> <td>idSession</td><td>Nom complet</td><td>Matricule</td><td>Classe</td><td>Numéro</td></tr>';
      for (var count=0; count < data.length;count++){
        html +='<tr>' ;
         html += '<td>'+data[count].idSession+'</td>'; 
         html += '<td>'+data[count].Nom+'</td>'; 

         html += '<td>'+data[count].matricule+'</td>'; 
         html += '<td>'+data[count].Classe+'</td>'; 
         html += '<td>'+data[count].numero+'</td>'; 
         html +='</tr>' ;
         };
     
         $('#tbd').html(html);
   


},

error: function (data) {
  alert("error");

console.log('Error:', data);
}
  })

  });

});
</script>
</html>