<!DOCTYPE html>
<html>
<head>
    <title>session</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    


<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.css"/>
  <script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script> 
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<style>

#col2{
    position:absolute;
    left:250px;
    top:100px;
    overflow:hidden;
    
}

pre{
  background-color:white;
  border:0;
}
.td{
  margin-left:100px;
}

</style>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <ul class="nav navbar-nav">
    @if(Auth::user()->isAdmin)<li class="nav-item"><a href="utilisateur">Utilisateur</a></li>@endif

    <li class="nav-item" id="id1"><a  class="nav-link" href="use">Etudiants</a></li>
    <li class="nav-item"><a href="filiere">Filières</a></li>
    <li class="nav-item"><a href="niveau">Niveaux</a></li>
    <li class="nav-item"><a href="departement">Départements</a></li>
    <li class="nav-item"><a href="salle">Salles</a></li>
    <li  class="nav-item active"><a href="session">Sessions</a></li>
    </ul>

    <ul class="nav navbar-nav navbar-right">
    <li>
   
   <a  href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
      <span class="glyphicon glyphicon-log-in"></span> Se Déconnecter
   </a>

   <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
       @csrf
   </form>

</li>
    </ul>
    </nav>
<button type="button" class="btn btn-white" Onclick=window.location.href='page' >
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-house-door-fill" viewBox="0 0 16 16"> <path d="M6.5 14.5v-3.505c0-.245.25-.495.5-.495h2c.25 0 .5.25.5.5v3.5a.5.5 0 0 0 .5.5h4a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4a.5.5 0 0 0 .5-.5z"/> </svg>
    </button>
<div id="alert_message"></div>

@if($errors->any())
    <div class="alert alert-danger">
    <ul>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
<span class="sr-only" >Close</span>
</button>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
    </ul>
    </div>
    @endif
  
  @if(Session::has('success'))
<div class="alert alert-success alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
<span class="sr-only" >Close</span>
</button>
<strong>{{ Session::get('success')}}</strong>
</div>
  @endif  

<div class="container">
<h1 align="center">Les Sessions</h1>
<br/>
<div class="row">
<div class="col-lg-12 margin-tb">

</div>
</div>

      <div class="pull-right">
<a class="btn btn-success mb-2" id="new-user" data-toggle="modal"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
  <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
  <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
</svg></a>
</div>
     <pre></pre>
    <pre></pre>
<div class="panel panel-primary">
      <div class="panel-heading" align="center">Liste Des Sessions</div>
      <div class="panel-body">
      <table  class="table table-striped table-bordered table-hover data-table">
        <thead>
            <tr>
                <th>Type session</th>
                <th>Date Début session</th>
                <th>Date Fin session</th>
                <th>Année session</th>
                <th width="200px">Action</th>
            </tr>
            </thead>
        <tbody >
        </tbody>
    </table>
      </div>
    </div>



    <div class="modal fade" id="attrM" tabindex="-1" aria-labelledby="studentModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
      
<button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="modal-title" id="attrLabel"></h5>
      </div>
      <div class="modal-body" >
    
      <form method="post" action="meme">
    @csrf
    <input type="hidden" id="id" name="id" />
    <div class="panel panel-primary">
      <div class="panel-heading" align="center">Cochér les filiéres qui passeront l'examen ensemble</div>
      <div class="panel-body">
    <table border="1" algin="center" class="table">
<tbody id="tbd">
</tbody>
</table>
</div>
</div>
      <div class="modal-footer">
      
      
            <a href="{{ route('session') }}" class="btn btn-danger">Annuler</a>
          <button  class="btn btn-primary ajaxstudent-save" >Enregistrer</button>
            </div>
            </div>
    </form>
</div>
</div>
</div>
</div>





    <div class="modal fade" id="studentModal" tabindex="-1" aria-labelledby="studentModalLabel" aria-hidden="true">
  <div class="modal-dialog ">
    <div class="modal-content">
      <div class="modal-header">
      
<button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="modal-title" id="studentModalLabel"></h5>
      
      </div>
      <div class="modal-body" >
    
      <form method="post" action="checkbox">
    @csrf
    <input type="hidden" id="id2" name="idS" />
        
    <div class="panel panel-primary">
      <input type="button" class="check btn btn-danger pull-right" value="Cochér tout" id="cocher"/>
      <div class="panel-heading" align="center">Paramétrage
      </div>
      <div class="panel-body">
        <table border="1" class="table">
        <tr>
<td width="10px">Départements</td>
<td width="150px">Filiéres</td>
<td >Niveaux</td>

        </tr>
        <?php
        for($i=0;$i<count($depar);$i++){
        $fil=\App\Models\filiere::where('NomDept','=',$depar[$i]->Nom)->get();
        ?>
<tr >
<td rowspan=<?php echo count($fil); ?> ><?php echo $depar[$i]->Nom; ?></td>
<?php

        for($j=0;$j<count($fil);$j++){?>
<th   ><?php echo $fil[$j]->Nom; ?></th>
<td >
<?php for($k=0;$k<count($niveau);$k++){
$cls=\App\Models\classe::where('NomFiliere','=',$fil[$j]->Nom)->where('NomNiveau','=',$niveau[$k]->Nom)->first();

?>
 <input type="checkbox"  id="td" width="90px" name="{{$cls->NomClasse}}" class="{{$cls->NomClasse}}" value="{{$cls->NomClasse}}"/>   <?php echo $niveau[$k]->Nom; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 
<?php } ?>
</td> </tr>
      <?php  } ?>
    

      <?php  } ?>
      
   
       
        </table>
       </div>
       </div>
       
      </div>
      <div class="modal-footer">
      
  
            <a href="{{ route('session') }}" class="btn btn-danger">Annuler</a>
          <button  class="btn btn-primary ajaxstudent-save" id="sessionP">Enregistrer</button>
            </div>
            </div>
      </form >

    </div>
  </div>
</div>



    <div class="modal fade" id="crud-modal" aria-hidden="true" >
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">

<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title" id="userCrudModal"></h4>
</div>
<div class="modal-body">
<form name="userForm" action="insertR" method="POST">
<input type="hidden" name="user_id" id="user_id" >
@csrf
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>Type session:</strong>					
<input type="text" name="type" id="nom" placeholder="Entrer le Nom du session" class="form-control"  />
						</div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
                        <strong>Date Début:</strong>					
<input type="date" name="dateD" id="dd" placeholder="Entrer le date début du session" class="form-control"  />
						</div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
                        <strong>Date Fin:</strong>					
<input type="date" name="dateF" id="df" placeholder="Entrer le date fin du session" class="form-control"  />
						</div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
                        <strong>Année:</strong>					
<input type="text" name="annee" id="an" placeholder="Entrer l'année " class="form-control"  />
						</div>
                        </div>        

<div class="col-xs-12 col-sm-12 col-md-12 text-center">

<button type="submit" id="btn-save" name="btnsave" class="btn btn-primary" >Enregistrer</button>
<a href="{{ route('session') }}" class="btn btn-danger">Annuler</a>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
   
</body>
   
<script type="text/javascript">
  $(function () {
    
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('session') }}",
        
        dom:'lBfrtip',
        buttons: [
            'excel', 'csv', 'pdf'
        ],
        "lengthMenu":[[3,5,10,25,50,100,-1],[3,5,10,25,50,100,"All"]],
        columns: [
            {data: 'type', name: 'type'},
            {data: 'dateDebut', name: 'dateDebut'},
            {data: 'dateFin', name: 'dateFin'},
            {data: 'annee', name: 'annee'},
           
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        "language":{
    "sProcessing":     "Traitement en cours...",
    "sSearch":         "Rechercher&nbsp;:",
    "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
    "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
    "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
    "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
    "sInfoPostFix":    "",
    "sLoadingRecords": "Chargement en cours...",
    "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
    "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
    "oPaginate": {
        "sFirst":      "Premier",
        "sPrevious":   "Pr&eacute;c&eacute;dent",
    
        "sNext":       "Suivant",
    
        "sLast":       "Dernier"
        }}
    });

$('#new-user').click(function () {
$('#btn-save').val("create-user");
$('#user').trigger("reset");
$('#userCrudModal').html("Ajouter un session");
$('#crud-modal').modal('show');
});

$('body').on('click', '#edit-user', function () {
var user_id = $(this).data('id');
$.get('session/'+user_id+'/edit', function (data) {
$('#userCrudModal').html("Modifier session");
$('#btn-update').val("Update");
$('#btn-save').prop('disabled',false);
$('#crud-modal').modal('show');
$('#user_id').val(data.id);
$('#nom').val(data.type);
$('#dd').val(data.dateDebut);
$('#df').val(data.dateFin);
$('#an').val(data.annee);

})
});

$('body').on('click', '#delete-user', function () {
var user_id = $(this).data("id");
var token = $("meta[name='csrf-token']").attr("content");
if(confirm("êtes Vous Sûr?")){

$.ajax({
    url: 'session/'+user_id,
type: 'DELETE',

data: {
"id": user_id,
"_token": token,
},
success: function (data) {
$('#alert_message').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span><span class="sr-only" >Close</span></button>session bien Supprimée<div>');

table.ajax.reload();
},
error: function (data) {
  $('#alert_message').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span><span class="sr-only" >Close</span></button>erreur dans la Suppréssion<div>');
console.log('Error:', data);
}
})
}
});


$('body').on('click', '#plus', function () {

$('.dropdown-menu').show();
  });

  $('body').on('click', '#close', function () {

$('.dropdown-menu').hide();
  });

$('body').on('click', '#repar', function () {
//alert("je suis la button repartition");
var user_id = $(this).data("id");
var token = $("meta[name='csrf-token']").attr("content");
//alert(user_id);

$.ajax({
    url: 'repartition',
type: 'GET',

/* data: {
"id": user_id,
"_token": token,
} */
})
});
$('body').on('click', '#parametrage', function () {
  var user_id = $(this).data('id');
  //alert(user_id);
$.get('session1/'+user_id+'/edit1', function (data) {

  



$('#studentModalLabel').html("Paramétrer une session");
$('#studentModal').modal('show');
$('#id2').val(user_id);
//$('#id1').val(data.idSession);
 for(var index=0;index<data.length;index++){
//alert(data[index].idSession);
//alert(data[index].NomClasse);
$('.'+data[index].NomClasse).prop('checked',true);

/*  foreach($classe as $cls){
 if( echo $cls->NomClasse; =='DI3')
  alert(data[index].NomClasse);
return false; */

 
} 

})

//})
});

$('body').on('click', '#cocher', function () {

<?php
        for($i=0;$i<count($depar);$i++){
        $fil=\App\Models\filiere::where('NomDept','=',$depar[$i]->Nom)->get();

        for($j=0;$j<count($fil);$j++){


 for($k=0;$k<count($niveau);$k++){
$cls=\App\Models\classe::where('NomFiliere','=',$fil[$j]->Nom)->where('NomNiveau','=',$niveau[$k]->Nom)->first();

?>
 $(".<?php echo $cls->NomClasse; ?>").prop('checked',true);
<?php } ?>

      <?php  } ?>
    

      <?php  } ?>

}); 

$('body').on('click', '#attr', function () {
  var user_id = $(this).data('id');
var token = $("meta[name='csrf-token']").attr("content");
   //   $('#tbd').html('');
       
$.ajax({
    url:'fetch/'+user_id,
    type:'GET',
    dataType:'json',

    success:function(data,data1){
 // data=JSON.parse(data);
//alert(data.NomClasse);
var html='';
      html += '<tr> <td>?</td>';
      for (var count=0; count < data.length;count++){
         html += '<td>'+data[count].NomClasse+'</td>'; };
      html +='</tr>' ;
      for(var count=0; count<data.length;count++){ 
        html += '<tr><td>' +data[count].NomClasse+ '</td>';
      for (var count1=0; count1<data.length;count1++) {
        if((data[count].NomClasse)==(data[count1].NomClasse)){
      html += '<td><input type="checkbox" name="'+data[count1].NomClasse+''+data[count].NomClasse+'" Onclick="return false" checked></td>';
    }
    else{
    html += '<td><input type="checkbox" name="'+data[count1].NomClasse+''+data[count].NomClasse+'" class="'+data[count1].NomClasse+''+data[count].NomClasse+'"></td>'; }
  };
    html +='</tr>';
    };

  /*   for (var count1=0; count1<data.length;count1++) {
      for (var count2=0; count2<data.length;count2++) {
        $('.'+data[index].filiere2).prop('checked',true);
      
      };
    };  */
 /*    for (var count2=0; count2 < data1.length;count2++){
        // html += '<td>'+data[count].NomClasse+'</td>'; 
        alert(data1[count2].filiere1);
         }; */
      
      $('#tbd').html(html);
         },
error: function (data) {
  alert('error');
console.log('Error:', data);
}
  })

   $.get('memet/'+user_id, function (data1) {


//$('#id1').val(data.idSession);
 for(var index=0;index<data1.length;index++){
//alert(data[index].idSession);
//alert(data[index].NomClasse);
//alert(+data1[index].filiere1+''+data1[index].filiere2);
$('.'+data1[index].filiere2+''+data1[index].filiere1).prop('checked',true);

/*  foreach($classe as $cls){
 if( echo $cls->NomClasse; =='DI3')
  alert(data[index].NomClasse);
return false; */

 }
   })


  $('#attrLabel').html("même temps");
$('#attrM').modal('show');
$('#id').val(user_id); 





});
  });
</script>
<script>
$(document).ready(function() {
  //hider les filiere et les niveaux de MQI: 

  <?php foreach($filiereMqi as $fil){ ?>
    $("#filiere ").hide();
    $(".<?php echo $fil->Nom; ?>#niveau").hide();
  <?php }?>

//hider les filiere et les niveaux de MED: 

<?php foreach($filiereMed as $fil){ ?>
    $("#filiereM ").hide();
    $(".<?php echo $fil->Nom; ?>#niveau").hide();
  <?php }?>
  //afficher ou hidder les fils de MQI:

  $('.MQI').click(function(event) {
    if ($(this).is(":checked")){
      <?php foreach($filiereMqi as $fil){ ?>
    $("#filiere ").show();
  <?php }?>
    }else {
      <?php foreach($filiereMqi as $fil){ ?>
    $("#filiere " ).hide();
    $("#filiere .<?php echo $fil->Nom; ?>").prop('checked',false);

    $(".<?php echo $fil->Nom; ?>#niveau").hide();
  <?php }?>
    }
  });

//afficher ou hidder les fils de MED:

$('.MED').click(function(event) {
    if ($(this).is(":checked")){
      <?php foreach($filiereMed as $fil){ ?>
    $("#filiereM ").show();
  <?php }?>
    }else {
      <?php foreach($filiereMed as $fil){ ?>
    $("#filiereM " ).hide();
    $("#filiereM .<?php echo $fil->Nom; ?>").prop('checked',false);
    $(".<?php echo $fil->Nom; ?>#niveau").hide();

  <?php }?>
    }
  });

/* afficher ou hidder le niveaux de MQI */

  <?php foreach($filiereMqi as $fil){ ?>
    $("#filiere .<?php echo $fil->Nom; ?>").click(function(event) {
      if($(this).is(":checked")){
    $(".<?php echo $fil->Nom; ?>#niveau").show();
      }
      else {
        $(".<?php echo $fil->Nom; ?>#niveau").hide();
  
      }
    });
    <?php }?>

    /* afficher ou hidder le niveaux de MED */

  <?php foreach($filiereMed as $fil){ ?>
    $("#filiereM .<?php echo $fil->Nom; ?>").click(function(event) {
      if($(this).is(":checked")){
    $(".<?php echo $fil->Nom; ?>#niveau").show();
      }
      else {
        $(".<?php echo $fil->Nom; ?>#niveau").hide();
  
      }
    });
    <?php }?>

    //departement 2:
 
 
});

$(function(){
$('.check').on('click',function(){
  <?php foreach($depar as $dep) { ?>
  $(" .<?php echo $dep->Nom; ?>").prop('checked',true);
  <?php }?>

  <?php foreach($filiereMqi as $fil){ ?>
    $("#filiere " ).show();
    $("#filiere .<?php echo $fil->Nom; ?>").prop('checked',true);
    $(".<?php echo $fil->Nom; ?>#niveau").show();
    <?php }?>

    <?php foreach($niveau as $niv){ ?>
      $(" .<?php echo $niv->Nom; ?>").prop('checked',true);

<?php }?>

    <?php foreach($filiereMqi as $fil){ ?>
    $(".<?php echo $fil->Nom; ?>#niveau").prop('checked',true);
    <?php }?>
    
    <?php foreach($filiereMed as $fil){ ?>
    $("#filiereM " ).show();
    $("#filiereM .<?php echo $fil->Nom; ?>").prop('checked',true);
    $(".<?php echo $fil->Nom; ?>#niveau").show();
  <?php }?>

});
});

$(function(){
$('.uncheck').on('click',function(){
  <?php foreach($depar as $dep) { ?>
  $(" .<?php echo $dep->Nom; ?>").prop('checked',false);
  <?php }?>

  <?php foreach($filiereMqi as $fil){ ?>
    $("#filiere " ).hide();
    $("#filiere .<?php echo $fil->Nom; ?>").prop('checked',false);
    $(".<?php echo $fil->Nom; ?>#niveau").hide();
    <?php }?>

    <?php foreach($niveau as $niv){ ?>
      $(" .<?php echo $niv->Nom; ?>").prop('checked',false);

<?php }?>

    <?php foreach($filiereMqi as $fil){ ?>
    $(".<?php echo $fil->Nom; ?>#niveau").prop('checked',false);
    <?php }?>
    
    <?php foreach($filiereMed as $fil){ ?>
    $("#filiereM " ).hide();
    $("#filiereM .<?php echo $fil->Nom; ?>").prop('checked',false);
    $(".<?php echo $fil->Nom; ?>#niveau").hide();
  <?php }?>

});
});
</script>
</html>