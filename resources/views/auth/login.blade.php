@extends('layouts.app')

@section('content')
<div class="container">
<pre></pre>
<pre></pre>
<pre></pre>
<pre></pre>
<pre></pre>
<pre></pre>

<div class="d-flex justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
            <pre>

			 <h3 align="center">Connéctez-vous</h3></pre>
             </div>
                

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <label for="usr">
							E-Mail:
						</label>
					<div class="input-group form-group inner-addon right-addon">
						<input id="email" type="email" placeholder="Saisir votre gmail" class=" input form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
					</div>
                        

                    <label for="usr">
							Mot de passe:
							</label>
					<div class="input-group form-group inner-addon right-addon">
					
						<input id="password" type="password" class="input form-control @error('password') is-invalid @enderror" name="password" placeholder="Saisir le Mot de passe" required autocomplete="current-password" >
                        @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                    </div>     
                      

               
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                            <div class="form-group">
						<input type="submit" value="Connecter" class="btn float-right login_btn border-raduis">
					</div>

                             
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
