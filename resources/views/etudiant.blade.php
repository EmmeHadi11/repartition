<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>example </title>
    <!-- CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>

<!-- //jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<!-- alert -->
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- jsdelivary -->
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>
<!-- css delivary -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

<!--excel and pdf csv export -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script> -->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.css"/>
  <script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> -->
 
<!--JS DATATABLE -->
<!-- <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script> -->

<link href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css" rel="stylesheet" />




</head>
<body>
<div class="container" id="ajouter">

<table class="table-striped " border=1 cellspacing=3 cellpadding="5">
<div class="card"><tr>
<td colspan="3">
<div class="card-header">
<h4> Ajouter un etudiant </h4>

</div>
</td>
</tr>
<div class="card-body">
<form action="insert" method="post">
@csrf
<tr>
<td>

<div class="form-group">
<label for="">Num d'inscription</label>
<input type="text" name="matricule" placeholder="Entrer le Num" class="form-control"/>
</div>
</td>
<td>
<div class="form-group">
<label for="">Nom Complet</label>
<input type="text" name="nom" placeholder="Entrer le Nom" class="form-control"/>
</div>
</td>
<td>
<div class="form-group">
<label for="">NNI</label>
<input type="text" name="nni" placeholder="Entrer le NNI" class="form-control"/>
</div>
</td>
</tr>
<tr>
<td>
<div class="form-group">
        <label for=""> Date Naissance</label>
        <input type="date" class="form-control" name="dateN">
        </div>
        </td>
        <td>
        <div class="form-group">
        <label for=""> Lieu de Naissance</label>
        <input type="text" class="form-control" placeholder="Lieu de Naissance" name="lieuN">
        </div>
        </td>
        <td>
        <div class="form-group">
        <label for="">Sexe</label><br>
        <div >
        <input type="radio"  name="sex" value="femme">femme 
        <input type="radio"  name="sex" value="homme">homme
        </div>
        </div>
        </td>
</tr>
<tr>
<td>
<div class="form-group">
<label for="">Niveau</label><br>
<select name="choix">
    <option value="opt" class="form-control">choisir Niveau</option>
    @foreach($niveau as $niv)
   <option ><?php echo"$niv->NomN" ?></option>
    @endforeach
    </select>
    </div>
</td>
<td>
<div class="form-group">
<label for="">Filiere</label><br>
<select name="choixN">
    <option value="opt" class="form-control">choisir Niveau</option>
    @foreach($filiere as $fil)
   <option ><?php echo"$fil->NomF" ?></option>
    @endforeach
    </select>
    </div>
</td>
<td>
<div class="form-group">
<input type="submit"  Value="Soumettre" class="btn btn-primary"/>
</div>
</td>
</tr>
</form>
</div> 

</div>
</table>
</div>
<div class="container">
<div class="row">
<div class="col-md-12 my-4">
<h1 class="text-center">Liste des etudiants </h1>
</div>
<div class="col-md-12">
<div class="card">
<div class="card-header">
<h4> jquery ajax crud - student data </h4>
<a  id="ajoue" class="float-end"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
  <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
  <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
</svg></a>
</div> 
<div class="card-body"> 
<table id="example" class="table table-striped table-light">
    <thead>
    <tr>
    <th>num</th>
    <th>nom</th>
    <th>filier</th>
    <th>niveau</th>
    <th colspan=2>Operations</th>

    </tr>
    </thead>
    </table>
</div>
</div>
</div>
</div>

</div>
<script>
$(document).ready(function() {
    $('#ajouter').hide();
    $('#ajoue').click(function(){
        $('#ajouter').show();

    });
});
</script>

    <script>
$(document).ready(function() {
    
    var table= $('#example').DataTable({
        "lengthMenu":[[3,5,10,25,50,100,-1],[3,5,10,25,50,100,"All"]],
        "responsive":true,
        "lengthChange":true,
        //"order": [[ 1, 'asc' ]],
        "ServerSide":true,
        "bProcessing": true,
        "sAjaxSource": "/livetable/fetch_data",
        dom:'lBfrtip',
        buttons: [
            'excel', 'csv', 'pdf', 'copy'
        ],
        
        "aoColumns": [
              { mData: 'Num' } ,
              { mData: 'Nom' } ,
              { mData: 'filiere' } ,
              { mData: 'niveau' },
              
              { 
                 mData: 'id',
                 render: (data,type,row) => {
                   return `<button class="edit" >update</button> <button id="delete">delete</button>` ;
                 }
              }
            ],
        "language":{
    "sProcessing":     "Traitement en cours...",
    "sSearch":         "Rechercher&nbsp;:",
    "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
    "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
    "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
    "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
    "sInfoPostFix":    "",
    "sLoadingRecords": "Chargement en cours...",
    "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
    "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
    "oPaginate": {
        "sFirst":      "Premier",
        "sPrevious":   "Pr&eacute;c&eacute;dent",
    
        "sNext":       "Suivant",
    
        "sLast":       "Dernier"
        }}
        
        
        
      });

      $('#example .edit').on('click',function(){ 
$.ajax({
    url: "edit",
    dataType:"JSON",
    data:{id:id},
    success:function(data){
        alert("delete with success");
        
    }
});

});

    $(document).on('click','#delete',function(){ 
        
//.. your logic for delete button click
var id =$(this).attr("id"); 
$.ajax({
    url: "{{route ('deleteE')}}" ,
    method:"POST",
    dataType:"json",
    data:{id:id, _token:_token},
    success:function(data){
        alert("delete with success");
    }
})
});

} );
</script>
<script>
$(document).ready( function() {
    $(document).on('click','.ajaxstudent-save', function() {
        alert("Hello i am here");
    } );
});
</script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>

</body>
</html>

