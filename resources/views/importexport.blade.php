<!DOCTYPE html>
<html>
<head>
    <title>Import Export Example</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
</head>
<body>
	<div class="container">
    <div class="card bg-light mt-3">
        <div class="card-header">
            Import Export Example
        </div>
        <div class="card-body">
            <form action="{{ route('export') }}" method="GET" enctype="multipart/form-data">
                {{ csrf_field() }}
                <select name="salle">
                    @foreach($salles as $sal)
                    <option class="form-control" value="{{$sal->idSession}}">{{$sal->Nom}}</option>
                    @endforeach
                </select>
                <br>
                <select  name="session">
                    @foreach($sessions as $ses)
                    <option class="form-control" value="{{$ses->id}}">{{$ses->type}}</option>
                    @endforeach
                </select>
                <input type="hidden" name="idd" value="2" class="form-control">
                <br>
                <button class="btn btn-warning" type="submit">Export Bulk Data</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>
