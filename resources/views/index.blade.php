<!DOCTYPE html>
<html>
  <head>
    <title>Insert Update and Delete record with AJAX in Laravel</title>
    <!-- provide the csrf token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> --> <!-- jQuery CDN -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>

<!-- //jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<!-- alert -->
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- jsdelivary -->
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>
<!-- css delivary -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

<!--excel and pdf csv export -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script> -->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.css"/>
  <script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> -->
 
<!--JS DATATABLE -->
<!-- <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script> -->

<link href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
  </head>
  <body>

    <table border='1' id='userTable' style='border-collapse: collapse;'>
      <thead>
        <tr>
          <th>Username</th>
          <th>Name</th>
          <th>Email</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><input type='text' id='username'></td>
          <td><input type='text' id='name' ></td>
          <td><input type='text' id='email' ></td>
          <td><input type='button' id='adduser' value='Add'></td>
        </tr>
      </tbody>
    </table>

<!-- Script -->
<script type='text/javascript'>
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

$(document).ready(function(){

  // Fetch records
  fetchRecords();

  // Add record
  $('#adduser').click(function(){

    var username = $('#username').val();
    var name = $('#name').val();
    var email = $('#email').val();

    if(username != '' && name != '' && email != ''){
      $.ajax({
        url: 'addUser',
        type: 'post',
        data: {_token: CSRF_TOKEN,username: username,name: name,email: email},
        success: function(response){

          if(response > 0){
            var id = response;
            var findnorecord = $('#userTable tr.norecord').length;

            if(findnorecord > 0){
              $('#userTable tr.norecord').remove();
            }
            var tr_str = "<tr>"+
            "<td align='center'><input type='text' value='" + username + "' id='username_"+id+"' disabled ></td>" +
            "<td align='center'><input type='text' value='" + name + "' id='name_"+id+"'></td>" +
            "<td align='center'><input type='email' value='" + email + "' id='email_"+id+"'></td>" +
            "<td align='center'><input type='button' value='Update' class='update' data-id='"+id+"' ><input type='button' value='Delete' class='delete' data-id='"+id+"' ></td>"+
            "</tr>";

            $("#userTable tbody").append(tr_str);
          }else if(response == 0){
            alert('Username already in use.');
          }else{
            alert(response);
          }

          // Empty the input fields
          $('#username').val('');
          $('#name').val('');
          $('#email').val('');
        }
      });
    }else{
      alert('Fill all fields');
    }
  });

});

// Update record
$(document).on("click", ".update" , function() {
  var edit_id = $(this).data('id');

  var name = $('#name_'+edit_id).val();
  var email = $('#email_'+edit_id).val();

  if(name != '' && email != ''){
    $.ajax({
      url: 'updateUser',
      type: 'post',
      data: {_token: CSRF_TOKEN,editid: edit_id,name: name,email: email},
      success: function(response){
        alert(response);
      }
    });
  }else{
    alert('Fill all fields');
  }
});

// Delete record
$(document).on("click", ".delete" , function() {
  var delete_id = $(this).data('id');
  var el = this;
  $.ajax({
    url: 'deleteUser/'+delete_id,
    type: 'get',
    success: function(response){
      $(el).closest( "tr" ).remove();
      alert(response);
    }
  });
});

// Fetch records
function fetchRecords(){
  $.ajax({
    url: 'getUsers',
    type: 'get',
    dataType: 'json',
    success: function(response){

      var len = 0;
      $('#userTable tbody tr:not(:first)').empty(); // Empty <tbody>
      if(response['data'] != null){
        len = response['data'].length;
      }

      if(len > 0){
        for(var i=0; i<len; i++){

          var id = response['data'][i].id;
          var username = response['data'][i].username;
          var name = response['data'][i].name;
          var email = response['data'][i].email;

          var tr_str = "<tr>" +
          "<td align='center'><input type='text' value='" + username + "' id='username_"+id+"' disabled></td>" +
          "<td align='center'><input type='text' value='" + name + "' id='name_"+id+"'></td>" + 
          "<td align='center'><input type='email' value='" + email + "' id='email_"+id+"'></td>" +
          "<td align='center'><input type='button' value='Update' class='update' data-id='"+id+"' ><input type='button' value='Delete' class='delete' data-id='"+id+"' ></td>"+
          "</tr>";

          $("#userTable tbody").append(tr_str);

        }
      }else{
        var tr_str = "<tr class='norecord'>" +
        "<td align='center' colspan='4'>No record found.</td>" +
        "</tr>";

        $("#userTable tbody").append(tr_str);
      }

    }
  });
}
</script>

  </body>
</html>