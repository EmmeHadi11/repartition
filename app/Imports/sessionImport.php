<?php

namespace App\Imports;

use App\Models\session;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class sessionImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new session([
            //
            'type' => $row['type'],
            'dateDebut' => $row['dated'],
            'dateFin' => $row['datef'],
            'annee' => $row['annee'],
        ]);
    }
}
