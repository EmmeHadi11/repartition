<?php

namespace App\Imports;

use App\Models\eleve;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
/* use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading; */

//use Throwable;


 class  eleveImport implements ToModel,WithHeadingRow
 /* SkipsOnError,
 WithValidation,
 SkipsOnFailure
 */

 
{
  // use Importable,SkipsErrors,SkipsFailures;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new eleve([
            //
            
            'NomComplet' => $row['nomcomplet'],
            'NumInscr' => $row['numinscr'],
            'DateNaiss' => $row['datenaiss'],
            'LieuNaiss' => $row['lieunaiss'],
            'filiere' => $row['filiere'],
            'niveau' => $row['niveau'],
            'sexe' => $row['sexe'],
            'NNI' => $row['nni'],
        ]);
    }
  /*     public function rules():array
     {
         return[
             'nomcomplet' =>['required','alpha'],
             'NumInscr'=>['alpha'],
         ];
 
    } */

}
