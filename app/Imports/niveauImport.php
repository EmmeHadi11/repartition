<?php

namespace App\Imports;

use App\Models\niveau;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class niveauImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new niveau([
            //
            'Nom'=>$row['nom'],
        ]);
    }
}
