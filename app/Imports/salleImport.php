<?php

namespace App\Imports;

use App\Models\salle;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class salleImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new salle([
            //
            'Nom'=>$row['nom'],
            'capacite'=>$row['capacite'],
        ]);
    }
}
