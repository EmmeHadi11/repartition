<?php

namespace App\Exports;

use App\Models\departement;
use Maatwebsite\Excel\Concerns\FromCollection;

class departementE implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return departement::all();
    }
    public function headings():array{
        return[
            'id',
            'Nom'
        ];
    }
}
