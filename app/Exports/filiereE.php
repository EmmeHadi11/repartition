<?php

namespace App\Exports;

use App\Models\filiere;
use Maatwebsite\Excel\Concerns\FromCollection;

class filiereE implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return filiere::all();
    }
    public function headings():array{
        return[
            'Nom',
            'id'
        ];
    }
}
