<?php

namespace App\Exports;

use App\Models\eleve;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class eleveE implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return eleve::all();
       // return collect(eleve::getEleve());
      
    }
    public function headings():array{
        return[
            'NomComplet',
            'NumInscr',
            'DateNaiss',
            'LieuNaiss',
            'filiere',
            'niveau',
            'sexe',
            'NNI'
        ];
    }
    
    
}

