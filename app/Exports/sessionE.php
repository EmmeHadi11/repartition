<?php

namespace App\Exports;

use App\Models\session;
use Maatwebsite\Excel\Concerns\FromCollection;

class sessionE implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return session::all();
    }
    public function headings():array{
        return[
            'id',
            'type',
            'Datedebut',
            'DateFin',
            'annee'
            
        ];
    }
}
