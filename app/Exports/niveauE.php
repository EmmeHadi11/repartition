<?php

namespace App\Exports;

use App\Models\niveau;
use Maatwebsite\Excel\Concerns\FromCollection;

class niveauE implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return niveau::all();
    }
    public function headings():array{
        return[
            'Nom',
            'id'
        ];
    }
}
