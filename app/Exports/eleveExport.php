<?php

namespace App\Exports;

use App\eleve;
use Maatwebsite\Excel\Concerns\FromCollection;

class eleveExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return eleve::all();
    }
}
