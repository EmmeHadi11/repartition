<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class use1 extends Model
{
   // use HasFactory;
   protected $table='use';
   public $timestamps=false;
   protected $fillable=['name','email'];
}
