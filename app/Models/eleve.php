<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class eleve extends Model
{
    //use HasFactory;
    protected $table='eleves';
    public $timestamps=false;
    protected $fillable=['NomComplet','NumInscr','DateNaiss','LieuNaiss','filiere','niveau','sexe','NNI','photo'];
    //use SoftDeletes;

    public static function getEleve(){

        $records =DB::table('eleves')->select('NomComplet','NumInscr','DateNaiss','LieuNaiss','filiere','niveau','sexe','NNI')->get()->toArray();
        return $records;
    }
}
