<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class sessionsalle extends Model
{
   // use HasFactory;
   protected $table='sessionsalle';
   protected $fillable=['idSession','Nom','capaciteUtilise','numDebut','numFin'];
   public $timestamps=false;
}
