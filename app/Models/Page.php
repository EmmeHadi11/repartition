<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    public static function getuserData($id=null){

        $value=DB::table('use')->orderBy('id', 'asc')->get(); 
        return $value;
   
      }
   
      public static function insertData($data){
   
        $value=DB::table('use')->where('username', $data['username'])->get();
        if($value->count() == 0){
          $insertid = DB::table('use')->insertGetId($data);
          return $insertid;
        }else{
          return 0;
        }
   
      }
   
      public static function updateData($id,$data){
         DB::table('use')->where('id', $id)->update($data);
      }
   
      public static function deleteData($id=0){
         DB::table('use')->where('id', '=', $id)->delete();
      }
   
   
}
