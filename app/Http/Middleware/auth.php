<?php

namespace App\Http\Middleware;

use Closure;
use PDO;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (create()->guest()) {
            return redirect('users');
        }
        return $next($request);
    }
}
