<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
       /* if (! $request->expectsJson()) {
           // return route('login');
          // return route('users');
          return redirect('users');
       }*/
    }
    public function handle($request, Closure $next){
       // dump('Auth Middleware');
       if(Auth::guest()){
return redirect('login');

       }
       else{
return $next($request);


       }
       
    }
}
