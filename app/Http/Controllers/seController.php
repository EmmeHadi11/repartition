<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\session;
use App\Models\parametrage;
use DataTables;
use Redirect,Response;

class seController extends Controller
{
    //
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = session::select('*');
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
     
                           $btn = '<a  id="edit-user" class="btn btn-success" data-toggle="modal" data-id='.$row->id.'><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-pencil" viewBox="0 0 16 16">
                           <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
                         </svg> </a>
                           <meta name="csrf-token" content="{{ csrf_token() }}">
                           <a id="delete-user" data-id='.$row->id.' class=" delete-user btn btn-danger"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16"> <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/> <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/> 
                           </svg></a>
                           <a  id="parametrage"  data-id='.$row->id.' class="btn btn-info">
                           <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-gear" viewBox="0 0 16 16"> <path d="M8 4.754a3.246 3.246 0 1 0 0 6.492 3.246 3.246 0 0 0 0-6.492zM5.754 8a2.246 2.246 0 1 1 4.492 0 2.246 2.246 0 0 1-4.492 0z"/> <path d="M9.796 1.343c-.527-1.79-3.065-1.79-3.592 0l-.094.319a.873.873 0 0 1-1.255.52l-.292-.16c-1.64-.892-3.433.902-2.54 2.541l.159.292a.873.873 0 0 1-.52 1.255l-.319.094c-1.79.527-1.79 3.065 0 3.592l.319.094a.873.873 0 0 1 .52 1.255l-.16.292c-.892 1.64.901 3.434 2.541 2.54l.292-.159a.873.873 0 0 1 1.255.52l.094.319c.527 1.79 3.065 1.79 3.592 0l.094-.319a.873.873 0 0 1 1.255-.52l.292.16c1.64.893 3.434-.902 2.54-2.541l-.159-.292a.873.873 0 0 1 .52-1.255l.319-.094c1.79-.527 1.79-3.065 0-3.592l-.319-.094a.873.873 0 0 1-.52-1.255l.16-.292c.893-1.64-.902-3.433-2.541-2.54l-.292.159a.873.873 0 0 1-1.255-.52l-.094-.319zm-2.633.283c.246-.835 1.428-.835 1.674 0l.094.319a1.873 1.873 0 0 0 2.693 1.115l.291-.16c.764-.415 1.6.42 1.184 1.185l-.159.292a1.873 1.873 0 0 0 1.116 2.692l.318.094c.835.246.835 1.428 0 1.674l-.319.094a1.873 1.873 0 0 0-1.115 2.693l.16.291c.415.764-.42 1.6-1.185 1.184l-.291-.159a1.873 1.873 0 0 0-2.693 1.116l-.094.318c-.246.835-1.428.835-1.674 0l-.094-.319a1.873 1.873 0 0 0-2.692-1.115l-.292.16c-.764.415-1.6-.42-1.184-1.185l.159-.291A1.873 1.873 0 0 0 1.945 8.93l-.319-.094c-.835-.246-.835-1.428 0-1.674l.319-.094A1.873 1.873 0 0 0 3.06 4.377l-.16-.292c-.415-.764.42-1.6 1.185-1.184l.292.159a1.873 1.873 0 0 0 2.692-1.115l.094-.319z"/> </svg></a>
                          
                           <a class=" dropdown pull-right btn btn-danger" id="dropdown-menu-'.$row->id.'" data-toggle="dropdown" data-boundary="viewport" aria-haspopup="true" aria-expanded="false">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-three-dots-vertical" viewBox="0 0 16 16">
                         <path d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"/>
                       </svg>
                           </a>
                           <div class="dropdown-menu pull-right" style="top:15px;" aria-labelledby="dropdown-menu-'.$row->id.'">
                           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                           <span aria-hidden="true">&times;</span>
                           <span class="sr-only" >Close</span>
                           </button><br>
                                   <a class="dropdown-item"  style="vertical-align: -35%;"  id="attr"  data-id='.$row->id.'>
                                   Même Temps        
                                   </a>
      <br>

                                  
                                   <a class="dropdown-item"  href="attr/'.$row->id.'/" id="sal"  data-id='.$row->id.'>
                                   Attribution Salle
                                   </a>
                                   
      <br>

                                   <a class="dropdown-item" href="repartition/'.$row->id.'/" id="repar"  data-id='.$row->id.' >
                                   Répartition
                                   </a>
                           </div>
                      ';
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
       
        $depar=\App\Models\departement::all();
        $classe=\App\Models\classe::all();
        $para=\App\Models\parametrage::all();
    $filiereMqi=\App\Models\filiere::where('NomDept','=','MQI')->get();
    $filiereMed=\App\Models\filiere::where('NomDept','=','MED')->get();
    $niveau=\App\Models\niveau::orderBy('Nom','ASC')->get();
    $salle=\App\Models\salle::all();
    $session=\App\Models\session::all();
    $classe=\App\Models\classe::all();
    return view('session',compact('classe','session','filiereMed','depar','filiereMqi','niveau','salle','classe','para'));
    }

    public function store(Request $request)
{

$r=$request->validate([
    'type' =>'required|min:2|alpha',
    'dateD' =>'required|date',
    'dateF' =>'required|date',
    'annee'=>'required',
]);
$anne=$request->annee;
$anne1=$anne+1;
$anne2=$anne."-".$anne1;

$uId = $request->user_id;
session::updateOrCreate(['id' => $uId],['type' => $request->type,'dateDebut'=>$request->dateD,
'dateFin'=>$request->dateF, 
'annee'=> $anne2]);
if(empty($request->user_id))
$msg = 'Session a été ajouté avec succès';
else
$msg = 'Il y avait une erreur';
return redirect()->route('session')->with('success',$msg);
//return view('session',compact('msg'));
}

public function edit($id)
{
$where = array('id' => $id);
$user = session::where($where)->first();
return Response::json($user);
}

public function destroy($id)
{
    
$user = session::where('id',$id)->delete();
return Response::json($user);
return redirect()->route('session');

}

public function parametrage(Request $request){
    dd($request->all());

}

public function verification(Request $req){
    $para=new \App\Models\parametrage;
    $id=$req->id;
    $res=\App\Models\parametrage::where('idSession','=',$id);
    return redirect()->route('session',compact('res'));
    
    

}
public function edit1($id)
{
    $where = array('idSession' => $id);
    $user = parametrage::where($where)->get();
    return Response::json($user);
}

/* public function edit2($id)
{
    //$id=$req->id;
 
    $user1 = \App\Models\numExamen::where('idSession','=',$id)->get();
    return Response::json($user1);
} */

public function repartition(Request $request){
    $id=$request->id;
    $passer=\App\Models\parametrage::where('idSession', '=', $id)->get();
    foreach($passer as $ps){
        $classe=\App\Models\classe::where('NomClasse', '=' ,$ps->NomClasse)->first();
        $count=\App\Models\eleves::where('filiere','=',$classe->NomFiliere)->where('niveau','=',$classe->NomNiveau)->count('id');
    echo ($classe->NomFiliere).($classe->NomNiveau);
    echo $count;    
    }
        

}
public function test(){
/*     $passer=\App\Models\parametrage::where('idSession', '=', '12')->get();
    foreach($passer as $ps){
        $classe=\App\Models\classe::where('NomClasse', '=' ,$ps->NomClasse)->first();
        $count=\App\Models\eleve::where('filiere','=',$classe->NomFiliere)->where('niveau','=',$classe->NomNiveau)->count('id');
    echo ($classe->NomFiliere).($classe->NomNiveau);
    echo"  count = ";
    echo $count;
    echo'<br>'; } */
    $j=0;
    $tableF=array();
    $passer1=\App\Models\memet::where('idSession', '=', '12')->first();
  //  foreach($passer1 as $ps1){
     
        echo"groupe 1 $passer1->filiere1   et groupe 2 est $passer1->filiere2 ";
        echo '<br>';
      //  for($i=0;$i<count($tableF);$i++){
// echo count($tableF);
 //if($tableF[$i]!=$passer1->filiere2){
     $tableF[$j]=$passer1->filiere1;
     
     $classe=\App\Models\classe::where('NomClasse', '=' ,$passer1->filiere1)->first();
     $count=\App\Models\eleve::where('filiere','=',$classe->NomFiliere)->where('niveau','=',$classe->NomNiveau)->count('id');
if($count==0)
echo"il y a aucun etudiant dans $passer1->filiere1<br>";
if($count==1)
echo"$passer1->filiere1 contient un seul eleve dont son numero est 1<br>";
else
echo "$passer1->filiere1 contient $count qui prend de 1 a $count<br>";
$j=$j+1;
$tableF[$j]=$passer1->filiere2;

$classe=\App\Models\classe::where('NomClasse', '=' ,$passer1->filiere2)->first();
$count2=\App\Models\eleve::where('filiere','=',$classe->NomFiliere)->where('niveau','=',$classe->NomNiveau)->count('id');
$countN=($count+1);
if($count2==0)
echo"il y a aucun etudiant dans $passer1->filiere2";
if($count2==1)
echo"$passer1->filiere2 contient un seul eleve dont son numero est $countN" ;
else
$count2N=$count2+1;
echo "$passer1->filiere2 contient $count2 qui prend de  $countN  a  $count2N";

$passer2=\App\Models\memet::where('idSession', '=', '12')->take(100000)->skip(1)->get();
foreach($passer2 as $ps2){
    $Nrept=0;
    for($k=0;$k<count($tableF);$k++){
    if($tableF[$k]!=$ps2->filiere1){
        $Nrept++;
    }
}
    if($Nrept!=0){
    $j=$j+1;
    $tableF[$j]=$ps2->filiere1;
    echo "<br> ";
    echo $tableF[$j];
    $Nrept2=0;
    for($k=0;$k<count($tableF);$k++){
        if($tableF[$k]!=$ps2->filiere2){
            $Nrept2++;
        }
    
}
if($Nrept2==count($tableF)){
    $j=$j+1;
    $tableF[$j]=$ps2->filiere2;
    echo "<br> ";
    echo $tableF[$j];}


} 
}
}

//$j=$j+1;
//echo $tableF[0];
// }
       // }
        
//    }
public function fetch_test($id){


   $where = array('idSession' => $id);
    $user = parametrage::where($where)->get();
    return Response::json($user);


}
 public function recupmemet($id){
   $where = array('idSession' => $id);
    $user1=\App\Models\memet::where($where)->get();
    return Response::json($user1);

 }  
    
}


