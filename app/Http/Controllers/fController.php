<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\filiere;
use DataTables;
use Session;
use Redirect,Response;

class fController extends Controller
{
    //
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = filiere::select('*');
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
     
                           $btn = '<a class="btn btn-success" id="edit-user" data-toggle="modal" data-id='.$row->id.'><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil" viewBox="0 0 16 16">
                           <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
                         </svg> </a>
                           <meta name="csrf-token" content="{{ csrf_token() }}">
                           <a id="delete-user" data-id='.$row->id.' class="btn btn-danger delete-user"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16"> <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/> <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/> 
                           </svg></a>
                           <a id="classe" data-id='.$row->id.' class="btn btn-info">Ajouter une classe</a>
                           ';
    
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
       
        $niveau=\App\Models\niveau::orderBy('Nom','ASC')->get();
        return view('filiere',compact('niveau'));
    }

    public function store(Request $request)
{

$r=$request->validate([
    'Nom' =>'required|min:2|alpha_num',
    'nomd'=>'required|min:2|alpha'
   
]);

$uId = $request->user_id;
$res=filiere::updateOrCreate(['id' => $uId],['Nom' => $request->Nom,'NomDept'=>$request->nomd]);
if($res){
    Session::flash('success','filiére a été ajouté avec succès');
           return redirect()->route('filiere');}
    else
    {
        Session::flash('error','il y avait une erreur');
               return redirect()->route('filiere');}
}



public function insert(Request $request)
{

$r=$request->validate([
    'choix'=>'required',
   
]);
$choix=$request->choix;
$log=strlen($choix);
$lastChar=$log-1;
$char=$choix[$lastChar];

$cls=new \App\Models\classe;
$cls->NomFiliere=$request->Nom;
$cls->NomNiveau=$request->choix;
$cls->NomClasse=($request->Nom).($char);
$res=$cls->save();


if($res){
    Session::flash('success','classe a été ajouté avec succès');
           return redirect()->route('filiere');}
    else
    {
        Session::flash('error','il y avait une erreur');
               return redirect()->route('filiere');}
}

public function edit($id)
{
$where = array('id' => $id);
$user = filiere::where($where)->first();
return Response::json($user);
}

public function classe($id)
{
$where = array('id' => $id);
$user = filiere::where($where)->first();
return Response::json($user);
}

public function destroy($id)
{
$user = filiere::where('id','=',$id)->delete();
return Response::json($user);
return redirect()->route('filiere');
}
}
