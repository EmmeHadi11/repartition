<?php

namespace App\Http\Controllers;
use PDO;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Excel;
use App\Imports\eleveImport;
use App\Imports\salleImport;
use App\Imports\filiereImport;
use App\Imports\niveauImport;
use App\Imports\departementImport;
use App\Exports\eleveE;
use App\Exports\salleE;
use App\Exports\sessionE;
use App\Exports\filiereE;
use App\Exports\niveauE;
use App\Exports\departementE;
use Validator;
use Importer;


class scrum1Controller extends Controller
{
    //
  
    public function getInfos(){

        return view('template');
    }


    public function verifie(Request $request){

        $login=$request->input('name');
        $pass=MD5($request->input('pass'));
     
$req=\App\Models\users::where('userName','=',$login)->where('password','=',$pass)->first();
        if(is_null($req)){
          Session::flash('error','Votre Nom d\'utilisateur ou Mot de passe est incorrecte ');
          return redirect('Auth');
        }
        
        else
        return redirect()->route('users.index');
    }
    

    public function ajouter(Request $req){
     
      
      $filiere=\App\Models\filiere::all();
      $niveau=\App\Models\niveau::all();
        return view('sal7',compact('filiere','niveau'));
        
        
    }
   
    public function insert(Request $request){
        //$bd=DB::getPdo();
        $eleve=new \App\Models\eleve;
        request()->validate([
          'nom' =>'required|min:2|alpha',
          'matricule' =>'required|min:2|alpha_num',
          'nni'=>'required|integer',
          'dateN'=>'required|date',
          'lieuN'=>'required|min:2|alpha',
          'choixN'=>'required',
          'choix'=>'required',
          'sex'=>'required'
        ]);
       $eleve->NomComplet=$request->input('nom');
       $eleve->NumInscr=$request->input('matricule');
       $eleve->NNI=$request->input('nni');
       $eleve->DateNaiss=$request->input('dateN');
       $eleve->lieuNaiss=$request->input('lieuN');
       $eleve->filiere=$request->input('choixN');
       $eleve->niveau=$request->input('choix');
       $eleve->sexe=$request->input('sex');

       $res=$eleve->save();
       if($res)
       return redirect('aceuill')->with("success","Etudiant bien Ajouter");
       else
       return redirect('aceuill')->with("echec","Echec de l'ajout de l'etudiant"); 
       
    }
  
      public function chercher(Request $req){
   
            $char=$req->input('char');
            $eleve=\App\Models\eleve::where("NomComplet" ,"like","%$char%")->orwhere("NumInscr" ,"like","%$char%")->orwhere("filiere" ,"like","%$char%")->orwhere("niveau" ,"like","%$char%")->orwhere("sexe" ,"like","%$char%")->orwhere("NNI" ,"like","%$char%")->orwhere("DateNaiss" ,"like","%$char%")->orwhere("LieuNaiss" ,"like","%$char%")->paginate(5);
            $filiere=\App\Models\filiere::all();
            $niveau=\App\Models\niveau::all();
            return view('sal7',compact('eleve','filiere','niveau'));
        
    }
    
    public function softdelete(\App\Models\eleve $eleve, Request $request) {
      $eleve->delete();
      return redirect('aceuill');
  }

  public function edit(\App\Models\eleve $eleve, Request $request) {
    $filiere = \App\Models\filiere::all();
    $niveau = \App\Models\niveau::all();
    return view('modifier', compact('eleve','filiere','niveau'));
}

public function update(\App\Models\eleve $eleve, Request $request) {

  request()->validate([
    'nom' =>'required|min:2|alpha',
    'num' =>'required|min:2|alpha_num',
    'nni'=>'required|integer',
    'dateN'=>'required|date',
    'lieuN'=>'required|min:2|alpha',
    'choixN'=>'required',
    'choix'=>'required',
    'sex'=>'required'
  ]);
    $eleve->NomComplet = $request->input('nom');
    $eleve->NumInscr = $request->input('num');
    $eleve->DateNaiss = $request->input('dateN');
    $eleve->LieuNaiss = $request->input('lieuN');
    $eleve->filiere = $request->input('choixN');
    $eleve->niveau = $request->input('choix');
    $eleve->sexe = $request->input('sex');
    $eleve->NNI = $request->input('nni'); 
    $eleve->save();

    return redirect('aceuill');
}

//salle
public function insertS(Request $request){
  //$bd=DB::getPdo();
  $salle=new \App\Models\salle;
 request()->validate([
   'Nom' =>'required|min:2|alpha_num',
   'capacite' =>'required'
 ]);
 $salle->Nom=$request->input('Nom');
 $salle->capacite=$request->input('capacite');
 

 $res=$salle->save();
 if($res)
 return redirect('salle')->with("success","salle bien Ajouter");
 else
 return redirect('salle')->with("echec","Echec de l'ajout de salle"); 
 
}
public function chercherS(Request $req){
  
  $char=$req->input('char');
  $salle=\App\Models\salle::where("Nom" ,"like","%$char%")->orwhere("capacite" ,"like","%$char%")->paginate(5);
  return view('salle',compact('salle'));

}

public function softdeleteS(\App\Models\salle $salle, Request $request) {
$salle->delete();
return redirect('salle');
}
public function editS(\App\Models\salle $salle, Request $request) {
  return view('modifierS', compact('salle'));
}

public function updateS(\App\Models\salle $salle, Request $request) {
  request()->validate([
    'Nom' =>'required|min:2|alpha_num',
    'capacite' =>'required'
  ]);
  $salle->Nom = $request->input('Nom');
  $salle->capacite = $request->input('capacite');
 
  $salle->save();

  return redirect('salle');
}


//filiere

public function insertF(Request $request){
  //$bd=DB::getPdo();
  $filiere=new \App\Models\filiere;
  request()->validate([
    'Nom' =>'required|min:2|alpha',
    
  ]);
 $filiere->Nom=$request->input('Nom');

 $res=$filiere->save();
 if($res)
 return redirect('filiere')->with("success","filiere bien Ajouter");
 else
 return redirect('filiere')->with("echec","Echec de l'ajout du filiere"); 
 
}
public function chercherF(Request $req){
        
  $char=$req->input('char');
  $filiere=\App\Models\filiere::where("Nom" ,"like","%$char%")->paginate(5);
  return view('filiere',compact('filiere'));

}

public function softdeleteF(\App\Models\filiere $filiere, Request $request) {
$filiere->delete();
return redirect('filiere');
}
public function editF(\App\Models\filiere $filiere, Request $request) {
  return view('modifierF', compact('filiere'));
}


public function updateF(\App\Models\filiere $filiere, Request $request) {
  $filiere->Nom = $request->input('Nom');
  request()->validate([
    'Nom' =>'required|min:2|alpha'
   
  ]);
 
  $filiere->save();

  return redirect('filiere');
}

//niveau

public function insertN(Request $request){
  //$bd=DB::getPdo();
  $niveau=new \App\Models\niveau;
  request()->validate([
    'Nom' =>'required|min:2|alpha_num'
  ]);
 $niveau->Nom=$request->input('Nom');

 $res=$niveau->save();
 if($res)
 return redirect('niveau')->with("success","Niveau bien Ajouter");
 else
 return redirect('niveau')->with("echec","Echec de l'ajout du niveau"); 
 
}
public function chercherN(Request $req){
        
  $char=$req->input('char');
  $niveau=\App\Models\niveau::where("Nom" ,"like","%$char%")->paginate(5);
  return view('niveau',compact('niveau'));

}

public function softdeleteN(\App\Models\niveau $niveau, Request $request) {
$niveau->delete();
return redirect('niveau');
}
public function editN(\App\Models\niveau $niveau, Request $request) {
  return view('modifierN', compact('niveau'));
}

public function updateN(\App\Models\niveau $niveau, Request $request) {
  $niveau->Nom = $request->input('Nom');
  
  request()->validate([
    'Nom' =>'required|min:2|alpha_num'
  ]);
  $niveau->save();

  return redirect('niveau');
}

//session


public function insertR(Request $request){
  //$bd=DB::getPdo();
  $session=new \App\Models\session;
  request()->validate([
    'type' =>'required|min:2|alpha',
    'dateD' =>'required|date',
    'dateF' =>'required|date',
    'annee'=>'required|integer',
    
  ]);
  $anne=$request->input('annee');
  $anne1=$anne+1;
  $anne2=$anne."-".$anne1;
 $session->type=$request->input('type');
 $session->dateDebut=$request->input('dateD');
 $session->dateFin=$request->input('dateF');
 $session->annee=$anne2;

 $res=$session->save();
 if($res)
 return redirect('session')->with("success","session bien Ajouter");
 else
 return redirect('session')->with("echec","Echec de l'ajout du session"); 
 
}
public function chercherR(Request $req){
        
  $char=$req->input('char');
  $session=\App\Models\session::where("type" ,"like","%$char%")->orwhere("dateDebut" ,"like","%$char%")->orwhere("dateFin" ,"like","%$char%")->orwhere("annee" ,"like","%$char%")->paginate(5);
  return view('session',compact('session'));

}

public function softdeleteR(\App\Models\session $session, Request $request) {
$session->delete();
return redirect('session');
}
public function editR(\App\Models\session $session, Request $request) {
  return view('modifierR', compact('session'));
}

public function updateR(\App\Models\session $session, Request $request) {
  request()->validate([
    'type' =>'required|min:2|alpha',
    'dateD' =>'required|date',
    'dateF' =>'required|date',
    'annee'=>'required|integer',
    
  ]);
  $anne=$request->input('annee');
  $anne1=$anne+1;
  $anne2=$anne."-".$anne1;
 $session->type=$request->input('type');
 $session->dateDebut=$request->input('dateD');
 $session->dateFin=$request->input('dateF');
 $session->annee=$anne2;
 
  $session->save();

  return redirect('session');
}

public function parametrageS(\App\Models\session $session, Request $request){
  $filiere=\App\Models\filiere::all();
  $niveau=\App\Models\niveau::all();
  $master=\App\Models\master::all();
  $masterniv=\App\Models\masterniv::all();
        return view('parametrage',compact('session','filiere','niveau','master','masterniv'));
       
}

public function insertP(\App\Models\session $session,Request $request){
  //$bd=DB::getPdo();
  $parametrage=new \App\Models\parametrage;
 
  //$niv=$request->get('case1');
  //$fil=$request->get('case2');
  foreach($request->case1 as $nv){
  foreach($request->case2 as $fl){


   $parametrage->idS=$request->input('id');
   $parametrage->idF=$fl;
   $parametrage->idN=$nv;
   $parametrage->save;
 }
}
return redirect('session');
 

 
 }

 //import

 public function import(Request $request){
   request()->validate([
     'file'=>'required|file|mimes:xlsx'
   ]);
   $import=new eleveImport;
   $file=$request->file('file');
 $import= Excel::import(new eleveImport,$file);
  if( $import) {
    Session::flash('success','Importation a été fait avec succès');
   return redirect()->route('users.index');
  }
  else{
Session::flash('error','il y avait une erreur');
return redirect()->route('users.index');
  }
  // Excel::import(new eleveImport,$request->file);
/*   $err=$import->errors();
return view('erreur',compact('err'));
   //dd($import->errors());

  //return view('erreur','$err');

  foreach($import->failures() as $failure){
    $failure->row();
    $failure->attribute();
    $failure->errors();
    $failure->values(); */
  
   }
 

 public function importS(Request $request){
  Excel::import(new salleImport,$request->fileS);
  return redirect('salle');
}

public function importSe(Request $request){
  Excel::import(new sessionImport,$request->fileSe);
  return redirect('session');
}

public function importF(Request $request){
  Excel::import(new filiereImport,$request->fileF);
  return redirect('filiere');
}

public function importN(Request $request){
  Excel::import(new niveauImport,$request->fileN);
  return redirect('niveau');
}

public function importD(Request $request){
  Excel::import(new departementImport,$request->fileD);
  return redirect('departement');
}




 //export
 public function export(){
   return Excel::download(new eleveE , 'eleve.xlsx');
 }
 public function exportS(){
  return Excel::download(new salleE , 'salle.xlsx');
}
public function exportSe(){
  return Excel::download(new sessionE , 'session.xlsx');
}
public function exportF(){
  return Excel::download(new filiereE , 'filiere.xlsx');
}
public function exportN(){
  return Excel::download(new niveauE , 'niveau.xlsx');
}
public function exportD(){
  return Excel::download(new departementE , 'departement.xlsx');
}
 public function exportCSV(){
  return Excel::download(new eleveE , 'eleve.csv');
}


//departement
public function insertD(Request $request){
  //$bd=DB::getPdo();
$departement =new \App\Models\departement ;
request()->validate([
'Nom'=>'required|min:2|alpha'
]);
$departement->Nom=$request->input('Nom');
$res=$departement->save();
 if($res)
 return redirect('departement')->with("success","departement bien Ajouter");
 else
 return redirect('departement')->with("echec","Echec de l'ajout du departement"); 
}
public function chercherD(Request $req){    
  $char=$req->input('char');
  $departement=\App\Models\departement ::where("Nom" ,"like","%$char%")->paginate(5);
  return view('departement',compact('departement'));
}

public function softdeleteD(\App\Models\departement  $departement, Request $request) {
$departement->delete();
return redirect('departement');
}
public function editD(\App\Models\departement $departement, Request $request) {
  return view('modifierD', compact('departement'));
}

public function updateD(\App\Models\departement $departement, Request $request) {
  $departement->Nom = $request->input('Nom');
  request()->validate([
    'Nom'=>'required|min:2|alpha'
    ]);
  $departement->save();
  return redirect('departement');
}



//importNew



/* public function importExcel(Request $request){
  $validator=Validator::make($request->all(), [
'file'=>'required|mimes:xlsx,xls,csv|max:5000'
  ]);
  $file=file($request->file->getRealPath());
  $data=array_slice($file,1);

  $parts=(array_chunk($data,5000));

} */

public function test(){
  $niveau=\App\Models\niveau::all();
  $filiereMqi=\App\Models\filiere::where('NomDept','=','MQI')->get();
$dataN=array();
$dataF=array();
$dataC=array();
$i=0; $j=0; 
foreach($filiereMqi as $fil){
foreach($niveau as $niv){
  
$dataN[$i]=($fil->Nom).($niv->Nom);
$i++;
}
}

foreach($filiereMqi as $fil){
  $dataF[$j]=$fil->Nom;
$j++;
}


foreach($dataN as $dt){
      echo $dt.'<br>';
    }
    foreach($dataF as $dt1){
      echo $dt1.'<br>';
    }
}

public function insertCheckbox(Request $request){
  
    $niveau=\App\Models\niveau::all();
    
    $filiere=\App\Models\filiere::all();

    foreach($filiere as $fil){
  foreach($niveau as $niv){
    $cls=\App\Models\classe::where('NomFiliere','=',$fil->Nom)->where('NomNiveau','=',$niv->Nom)->first();
if($request->input($cls->NomClasse)){
  $checkbox= new \App\Models\parametrage;
  $reqTest=\App\Models\parametrage::where('NomClasse','=',$cls->NomClasse)->where('idSession','=',$request->input('idS'))->first();
 if(is_null($reqTest)){
  $checkbox->idSession=$request->input('idS');
  
  $checkbox->NomClasse=$cls->NomClasse;
  $checkbox->save();
 }
}
  }
    }
    Session::flash('success','paramétrage bien fait');
    return redirect('session'); 


  



}

public function insertMeme(Request $request){
  $id1=$request->input('id');
    $para=\App\Models\parametrage::where('idSession','=',$id1)->get();
    $req1=\App\Models\memet::where('idSession','=',$request->input('id'))->delete();

    foreach($para as $pr){
      foreach($para as $pr1){
  $name=($pr1->NomClasse).($pr->NomClasse);
  if($request->input($name)){
    if(($pr->NomClasse)!=($pr1->NomClasse)){
      $memet=new \App\Models\memet;
$req=\App\Models\memet::where('filiere1','=',$pr->NomClasse)->where('filiere2','=',$pr1->NomClasse)->where('idSession','=',$request->input('id'))->first();

if(is_null($req)){
$memet->filiere1=$pr->NomClasse;
      $memet->filiere2=$pr1->NomClasse;
      $memet->decision=1;
      $memet->idSession=$request->input('id');
      $memet->save();
}
    }
    }
  }
  }

  Session::flash('success','bien fait');
  return redirect('session');
}

}


