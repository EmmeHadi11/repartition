<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    //
    public function getInfos(){

        return view('template');
    }


    public function create(Request $request){

        $login=$request->input('name');
        $pass=MD5($request->input('pass'));
       //$user=new \App\Models\createUser;
        $bd=DB::getPdo();
        $requete=$bd->query("SELECT * from user where userName='$login' and password='$pass'");
        $resbd=$requete->fetch();

        $loginbd=$resbd[1];
        $passbd=$resbd[2];

        if($login==$loginbd && $pass==$passbd)
        return redirect('aceuill');
        else
        return view('template');
    }
    

    public function ajouter(Request $req){
     
      
      $filiere=\App\Models\filiere::all();
      $niveau=\App\Models\niveau::all();
        return view('ajouter',compact('filiere','niveau'));
        
        
    }
   
    public function insert(Request $request){
        //$bd=DB::getPdo();
        $eleve=new \App\Models\eleve;
       
       $eleve->NomComplet=$request->input('nom');
       $eleve->NumInscr=$request->input('matricule');
       $eleve->NNI=$request->input('nni');
       $eleve->DateNaiss=$request->input('dateN');
       $eleve->lieuNaiss=$request->input('lieuN');
       $eleve->filiere=$request->input('choixN');
       $eleve->niveau=$request->input('choix');
       $eleve->sexe=$request->input('sex');

       $res=$eleve->save();
       if($res)
       return redirect('aceuill')->with("success","Etudiant bien Ajouter");
       else
       return redirect('aceuill')->with("echec","Echec de l'ajout de l'etudiant"); 
       
    }
  
      public function chercher(Request $req){
        
            $char=$req->input('char');
            $infos=\App\Models\eleve::where("NomComplet" ,"like","%$char%")->orwhere("NumInscr" ,"like","%$char%")->orwhere("filiere" ,"like","%$char%")->orwhere("niveau" ,"like","%$char%")->orwhere("sexe" ,"like","%$char%")->orwhere("NNI" ,"like","%$char%")->orwhere("DateNaiss" ,"like","%$char%")->orwhere("LieuNaiss" ,"like","%$char%")->get();
            return view('sal7',compact('infos'));
        
    }
    
    public function softdelete(\App\Models\eleve $eleve, Request $request) {
      $eleve->delete();
      return redirect('aceuill');
  }

  public function edit(\App\Models\eleve $eleve, Request $request) {
    $filiere = \App\Models\filiere::all();
    $niveau = \App\Models\niveau::all();
    return view('modifier', compact('eleve', 'filiere','niveau'));
}

public function update(\App\Models\eleve $eleve, Request $request) {
    $eleve->NomComplet = $request->input('nom');
    $eleve->NumInscr = $request->input('num');
    $eleve->DateNaiss = $request->input('dateN');
    $eleve->LieuNaiss = $request->input('lieuN');
    $eleve->filiere = $request->input('choixN');
    $eleve->niveau = $request->input('choix');
    $eleve->sexe = $request->input('sex');
    $eleve->NNI = $request->input('nni'); 
    $eleve->save();

    return redirect('aceuill');
}
   
}
