<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Models\sessionsalle;
use DataTables;
use Session;

use Redirect,Response;

class controler1 extends Controller
{
    public function index(Request $request,$id)
    {$id1=$request->id;
      $where=array('idSession' => $id);
      if ($request->ajax()) {
          $data = sessionsalle::select('*')->where('idSession',$id1)->get();
          return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
     
                           $btn = '<a  class="btn btn-success" id="edit-user" data-toggle="modal" data-id='.$row->id.'><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-pencil" viewBox="0 0 16 16">
                           <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
                         </svg> </a>
                           <meta name="csrf-token" content="{{ csrf_token() }}">
                           <a id="delete-user" class="btn btn-danger" data-id='.$row->id.' class=" delete-user"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16"> <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/> <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/> 
                           </svg></a>';
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);

           
        }
             
        $salle=\App\Models\salle::all();
        $salles2=\App\Models\sessionsalle::where('idSession',$id)->get();
        $salles= DB::table('sessionsalle')->orderBy('id','desc')->take(1)->where('idSession',$id)->get();
        $para=\App\Models\parametrage::where('idSession',$id)->get();
        return view('AttributionS',compact('salle','salles','salles2','id','para'));  
    }
  

   /*  public function store(Request $request){
      $ss=new \App\Models\sessionsalle;
      $ss->Nom=$request->Nom;
      $ss->capaciteUtilise=$request->capacite;
      $ss->numDebut=$request->NumDebut;
      $ss->numFin=$request->NumFin;
     $res= $ss->save();
     if($res){
      return redirect('attr');}
   
     } */


     //begin
     public function store(Request $request)
{

$r=$request->validate([
    'Nom' =>'required|min:2',
    'capacite' =>'required',
    'numD' =>'required',
    'numF'=>'required',
]);

$def=($request->numF)-($request->numD);
if($def<=$request->capacite){
$uId = $request->user_id;
$res=sessionsalle::updateOrCreate(['id' => $uId],['Nom' => $request->Nom,'numDebut'=>$request->numD,
'numFin'=>$request->numF, 'idSession'=>$request->idS,
'capaciteUtilise'=> $request->capacite]);

if($res){
  Session::flash('success','Cette salle a été attribué pour cette session');
  return redirect('attr/'.$request->idS);

}
  else
  {
      Session::flash('error','il y avait une erreur');
      return redirect('attr/'.$request->idS);

            }

}

else{
  Session::flash('error','Capacité de la salle a été dépassée');
  return redirect('attr/'.$request->idS);
 // return view('AttributionS');
}

}

public function edit($id)
{
$where = array('id' => $id);
$user = sessionsalle::where($where)->first();
return Response::json($user);
}
///end

public function destroy($id)
{
    
$user = sessionsalle::where('id',$id)->delete();
return Response::json($user);
//return redirect()->route('session');

}



}
