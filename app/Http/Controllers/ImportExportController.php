<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Exports\BulkExport;
use App\Imports\BulkImport;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use DB;
class ImportExportController extends Controller
{
    /**
    * 
    */
    public function importExportView()
    {
       $salles=\App\Models\sessionsalle::all();
       $sessions=\App\Models\session::all();
       return view('importexport',compact('salles','sessions'));
    }
    
    public function export(Request $req) 
    {
       
       $idS=$req->idS;
       $idSa=$req->salle;
       $classe=$req->classe;

$salle=DB::table('sessionsalle')->where('Nom','=',$idSa)->get();

       $query=DB::table('eleves')->join('numExamen',function ($join){ 
          $join->on('matricule','=','NumInscr')->on('Nom','=','NomComplet');})->join('sessionsalle', function ($join) {
         $join->on('numExamen.idSession', '=', 'sessionsalle.idSession')
          ->on('numero','>=','numDebut')->on('numero','<=','numFin');
      })->distinct()->select('NumInscr','NomComplet','Classe','numero','numExamen.idSession')->where('sessionsalle.Nom','=',$idSa)->where('numExamen.idSession','=',$idS)->where('Classe','=',$classe)->distinct()->get();
      
      /* $query=DB::select("select distinct e.NumInscr, e.NomComplet,n.numero,n.idSession FROM eleve e,numExamen n,sessionsalle s WHERE n.matricule=e.NumInscr and n.idSession=s.idSession  and s.idSession=n.idSession and n.numero >= s.numDebut and n.numero <= s.numFin
      and s.Nom='$idSa' and n.idSession='$idS' "); */
       if(is_null($query)){
         echo '<div class="alert alert-success"> cette salle n\'est appartient ps á cette session </div>';
       
      }else{
         $pdf = PDF::loadView('eleve_pdf',compact('query','salle'));
       return $pdf->download('eleve_pdf.pdf');
       
      }
      
    }
}
